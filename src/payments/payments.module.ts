import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Folder, FolderSchema } from '../schemas/Folder.schema';
import { User, UserSchema } from '../schemas/User.schema';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { File, FileSchema } from 'src/schemas/File.schema';
import { Product, ProductSchema } from 'src/schemas/Product.schema';
import { PaymentsService } from './payments.service';
import { Payment, PaymentSchema } from 'src/schemas/Payment.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Payment.name, schema: PaymentSchema },
      {
        name: Folder.name,
        schema: FolderSchema,
      },
      {
        name: User.name,
        schema: UserSchema,
      },
      {
        name: File.name,
        schema: FileSchema,
      },
      { name: Product.name, schema: ProductSchema },
    ]),
  ],
  controllers: [],
  providers: [JwtService, UsersService, PaymentsService],
})
export class PaymentsModule {}
