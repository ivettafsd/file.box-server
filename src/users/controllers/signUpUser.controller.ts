import { Controller, Post, Body } from '@nestjs/common';
import { SignUpUserDto } from './../dto/User.dto';
import { User } from '../../schemas/User.schema';
import { AuthService } from './../auth.service';
import {
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiTags,
  ApiOperation,
} from '@nestjs/swagger';

@Controller('users')
@ApiTags('Users')
export class SignUpUserController {
  constructor(private authService: AuthService) {}

  @Post('/signup')
  @ApiOperation({ summary: 'Sign up a new user' })
  @ApiCreatedResponse({
    description: 'User successfully signed up',
    type: User,
  })
  @ApiConflictResponse({
    description: 'Such email already exists',
  })
  async signUpUser(@Body() signUpUserDto: SignUpUserDto) {
    await this.authService.signUp(signUpUserDto);
    const authorizedUser = await this.authService.signIn(signUpUserDto);
    return authorizedUser;
  }
}
