export declare class EmailDto {
    email: string;
    password: string;
    name: string;
    project: string;
    support: string;
}
