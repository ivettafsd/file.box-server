"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FilesService = void 0;
const common_1 = require("@nestjs/common");
const client_s3_1 = require("@aws-sdk/client-s3");
const config_1 = require("@nestjs/config");
const mongoose_1 = require("mongoose");
const mongoose_2 = require("@nestjs/mongoose");
const File_schema_1 = require("../schemas/File.schema");
const users_service_1 = require("../users/users.service");
let FilesService = class FilesService {
    constructor(configService, filesModel, usersService) {
        this.configService = configService;
        this.filesModel = filesModel;
        this.usersService = usersService;
        this.s3Client = new client_s3_1.S3Client({
            region: this.configService.getOrThrow('AWS_S3_REGION'),
        });
    }
    async uploadRemoteFile(fileName, file) {
        try {
            await this.s3Client.send(new client_s3_1.PutObjectCommand({
                Bucket: this.configService.getOrThrow('AWS_S3_BUCKET'),
                Key: fileName,
                Body: file,
            }));
            const url = `https://${this.configService.getOrThrow('AWS_S3_BUCKET')}.s3.amazonaws.com/${fileName}`;
            return url;
        }
        catch (error) {
            throw new Error(`Error uploading file to S3: ${error.message}`);
        }
    }
    async deleteRemoteFile(fileName) {
        try {
            const deletedFile = await this.s3Client.send(new client_s3_1.DeleteObjectCommand({
                Bucket: this.configService.getOrThrow('AWS_S3_BUCKET'),
                Key: fileName,
            }));
            return deletedFile;
        }
        catch (error) {
            throw new Error(`Error deleting file ${fileName} from S3: ${error.message}`);
        }
    }
    async create(createDto) {
        const createdFile = await this.filesModel.create(createDto);
        const updatedUser = await this.usersService.update(createdFile.owner, {
            $inc: { availableMemory: -createdFile.size },
        });
        if (!updatedUser) {
            throw new common_1.NotFoundException('User not found');
        }
        return createdFile;
    }
    async findOneAndUpdate(filter, updateDto) {
        const updatedFile = await this.filesModel.findOneAndUpdate(filter, updateDto, { new: true });
        if (!updatedFile) {
            throw new common_1.ForbiddenException('File not found or does not belong to the current user');
        }
        return updatedFile;
    }
    async findOneAndDelete(filter) {
        const foundFile = await this.findOne(filter);
        if (!foundFile) {
            throw new common_1.ForbiddenException('File not found or does not belong to the current user');
        }
        const fileName = filter._id.toString();
        await this.deleteRemoteFile(fileName);
        await this.filesModel.findOneAndDelete(filter);
        const updatedUser = await this.usersService.update(foundFile.owner, {
            $inc: { availableMemory: +foundFile.size },
        });
        if (!updatedUser) {
            throw new common_1.NotFoundException('User not found');
        }
        return foundFile;
    }
    async deleteSubfiles(fileIds) {
        for (const file of fileIds) {
            await this.findOneAndDelete({ _id: file._id });
        }
    }
    async updateSubfiles(fileIds, updateDto) {
        for (const file of fileIds) {
            await this.findOneAndUpdate({ _id: file._id }, updateDto);
        }
    }
    async find(filters) {
        return await this.filesModel.find(filters);
    }
    async findOne(filters) {
        if (!filters) {
            return null;
        }
        return await this.filesModel.findOne(filters);
    }
};
exports.FilesService = FilesService;
exports.FilesService = FilesService = __decorate([
    (0, common_1.Injectable)(),
    __param(1, (0, mongoose_2.InjectModel)(File_schema_1.File.name)),
    __metadata("design:paramtypes", [config_1.ConfigService,
        mongoose_1.Model,
        users_service_1.UsersService])
], FilesService);
//# sourceMappingURL=files.service.js.map