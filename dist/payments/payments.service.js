"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentsService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const Payment_schema_1 = require("../schemas/Payment.schema");
const stripe_1 = require("stripe");
const { STRIPE_SECRET_KEY, STRIPE_CUSTOMER, BASE_URL } = process.env;
const stripe = new stripe_1.default(STRIPE_SECRET_KEY);
let PaymentsService = class PaymentsService {
    constructor(paymentsModel) {
        this.paymentsModel = paymentsModel;
    }
    async create(paymentDto, productStripeId) {
        const createdPayment = await this.paymentsModel.create({
            ...paymentDto,
            _id: new mongoose_2.Types.ObjectId(),
            status: 'pending',
        });
        if (!createdPayment) {
            return null;
        }
        const session = await stripe.checkout.sessions.create({
            line_items: [{ price: productStripeId, quantity: paymentDto.amount }],
            mode: 'payment',
            payment_intent_data: {
                setup_future_usage: 'on_session',
            },
            customer: STRIPE_CUSTOMER,
            success_url: BASE_URL + `/products/buy/success/${createdPayment._id}`,
            cancel_url: BASE_URL + `/products/buy/failed/${createdPayment._id}`,
        });
        return session;
    }
    async find(filters = {}) {
        return await this.paymentsModel.find(filters);
    }
    async findOne(filters = {}) {
        if (!filters) {
            return null;
        }
        return await this.paymentsModel.findOne(filters);
    }
    async findOneAndUpdate(filter, updateDto) {
        const updatedPayment = (await this.paymentsModel
            .findOneAndUpdate(filter, updateDto, { new: true })
            .populate('product'));
        if (!updatedPayment) {
            throw new common_1.BadRequestException('Unable to find and update the payment');
        }
        return updatedPayment;
    }
    async successSession(paymentId) {
        const updatedPayment = await this.findOneAndUpdate({ _id: new mongoose_2.Types.ObjectId(paymentId) }, { status: 'success' });
        return updatedPayment;
    }
    async failedSession(paymentId) {
        const updatedPayment = await this.findOneAndUpdate({ _id: new mongoose_2.Types.ObjectId(paymentId) }, { status: 'error' });
        return updatedPayment;
    }
};
exports.PaymentsService = PaymentsService;
exports.PaymentsService = PaymentsService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, mongoose_1.InjectModel)(Payment_schema_1.Payment.name)),
    __metadata("design:paramtypes", [mongoose_2.Model])
], PaymentsService);
//# sourceMappingURL=payments.service.js.map