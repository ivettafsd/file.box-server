"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetFilesController = void 0;
const common_1 = require("@nestjs/common");
const currentUser_decorator_1 = require("../../users/decorators/currentUser.decorator");
const auth_guard_1 = require("../../guards/auth.guard");
const User_schema_1 = require("../../schemas/User.schema");
const files_service_1 = require("../files.service");
const File_dto_1 = require("../dto/File.dto");
const folders_service_1 = require("../../folders/folders.service");
const swagger_1 = require("@nestjs/swagger");
let GetFilesController = class GetFilesController {
    constructor(fileService, foldersService) {
        this.fileService = fileService;
        this.foldersService = foldersService;
    }
    async getFiles(keyword = '', user) {
        const filter = {
            $or: [
                { owner: user._id },
                { 'permissions.email': user.email, 'permissions.role': 'editor' },
            ],
        };
        const regexSearch = { $regex: keyword, $options: 'i' };
        const foundFolders = await this.foldersService.find({
            ...filter,
            title: regexSearch,
        });
        const foundFiles = await this.fileService.find({
            ...filter,
            name: regexSearch,
        });
        return {
            folders: foundFolders,
            files: foundFiles,
        };
    }
};
exports.GetFilesController = GetFilesController;
__decorate([
    (0, common_1.Get)('/'),
    (0, swagger_1.ApiOperation)({
        summary: 'Get all folders and files by keyword owned by the user and folders where the current user has permissions',
    }),
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiQuery)({
        name: 'keyword',
        description: 'Keyword',
        example: 'File',
        required: false,
    }),
    (0, swagger_1.ApiOkResponse)({
        description: 'List of folders and files retrieved based on the provided keyword.',
        type: File_dto_1.FilesResponseDto,
    }),
    (0, swagger_1.ApiUnauthorizedResponse)({
        description: 'Invalid token or refreshToken or the user is not authenticated.',
    }),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    __param(0, (0, common_1.Query)('keyword')),
    __param(1, (0, currentUser_decorator_1.CurrentUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, User_schema_1.User]),
    __metadata("design:returntype", Promise)
], GetFilesController.prototype, "getFiles", null);
exports.GetFilesController = GetFilesController = __decorate([
    (0, common_1.Controller)('files'),
    (0, swagger_1.ApiTags)('Files'),
    __metadata("design:paramtypes", [files_service_1.FilesService,
        folders_service_1.FoldersService])
], GetFilesController);
//# sourceMappingURL=getFiles.controller.js.map