import {
  Controller,
  UseGuards,
  Param,
  NotFoundException,
  ForbiddenException,
  Delete,
  Res,
} from '@nestjs/common';
import { AuthGuard } from '../../guards/auth.guard';
import { FoldersService } from './../folders.service';
import { CurrentUser } from 'src/users/decorators/currentUser.decorator';
import { User } from 'src/schemas/User.schema';
import { Types } from 'mongoose';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { ValidId } from '../decorators/validID.decorator';

@Controller('folders')
@ApiTags('Folders')
export class DeleteFolderController {
  constructor(private readonly foldersService: FoldersService) {}

  @Delete('/:id')
  @ApiOperation({
    summary: 'Delete a folder by its ID',
  })
  @ApiBearerAuth()
  @ApiParam({
    name: 'id',
    description: 'Folder ID',
    example: '66471a9ce46805482dd323a4',
  })
  @ApiOkResponse({
    description: 'Removed folder success',
  })
  @ApiNotFoundResponse({
    description: 'Folder not found or does not belong to the current user',
  })
  @ApiBadRequestResponse({
    description: 'Invalid folder ID',
  })
  @ApiForbiddenResponse({
    description: 'Operation forbidden: Cannot remove the root folder',
  })
  @ApiUnauthorizedResponse({
    description:
      'Invalid token or refreshToken or the user is not authenticated.',
  })
  @UseGuards(AuthGuard)
  async removeFolder(
    @Param() @ValidId() { id },
    @CurrentUser() user: User,
    @Res() res,
  ) {
    if (user.mainFolder.toString() === id) {
      throw new ForbiddenException(
        'Operation forbidden: Cannot remove the root folder',
      );
    }
    const filter = { _id: new Types.ObjectId(id), owner: user._id };
    const deletedFolder = await this.foldersService.findOneAndDelete(filter);
    if (!deletedFolder) {
      throw new NotFoundException(
        'Folder not found or does not belong to the current user',
      );
    }
    res.status(200).json({ message: 'Removed folder success' });
  }
}
