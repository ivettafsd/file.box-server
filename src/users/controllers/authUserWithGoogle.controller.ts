import { Controller, Get, UseGuards } from '@nestjs/common';
import { AuthGuard as AuthGoogleGuard } from '@nestjs/passport';
import { ApiTags, ApiOperation } from '@nestjs/swagger';

@Controller('users')
@ApiTags('Users')
export class AuthUserWithGoogleController {
  @Get('/google')
  @ApiOperation({ summary: 'Authenticate with Google' })
  @UseGuards(AuthGoogleGuard('google'))
  async googleAuth() {}
}
