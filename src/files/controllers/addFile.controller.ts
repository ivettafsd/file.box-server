import {
  Controller,
  MaxFileSizeValidator,
  ParseFilePipe,
  Post,
  UploadedFile,
  UseInterceptors,
  UseGuards,
  Body,
  NotFoundException,
  ForbiddenException,
  ConflictException,
} from '@nestjs/common';
import { Types } from 'mongoose';
import { CurrentUser } from 'src/users/decorators/currentUser.decorator';
import { FileInterceptor } from '@nestjs/platform-express';
import { AuthGuard } from '../../guards/auth.guard';
import { User } from 'src/schemas/User.schema';
import { FilesService } from './../files.service';
import { CreateFileDto } from './../dto/File.dto';
import { FoldersService } from 'src/folders/folders.service';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiBody,
  ApiConflictResponse,
  ApiConsumes,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { File } from 'src/schemas/File.schema';

@Controller('files')
@ApiTags('Files')
export class AddFileController {
  constructor(
    private fileService: FilesService,
    private foldersService: FoldersService,
  ) {}

  @Post()
  @ApiOperation({ summary: 'Add a new file' })
  @ApiBearerAuth()
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        name: {
          description: 'File name',
          type: 'string',
          example: 'My file',
        },
        isPrivate: {
          description: 'Indicates whether the file is private',
          type: 'boolean',
          example: true,
        },
        rootFolder: {
          description: 'Root folder containing this file',
          type: 'string',
          example: '66471a9ce46805482dd323a4',
        },
        file: {
          description: 'File',
          type: 'string',
          format: 'binary',
        },
      },
      required: ['name', 'rootFolder', 'file'],
    },
  })
  @ApiCreatedResponse({
    description: 'File successfully created',
    type: File,
  })
  @ApiBadRequestResponse({
    description: 'Validation failed (expected size is less than 5000000)',
  })
  @ApiNotFoundResponse({
    description: 'Folder not found',
  })
  @ApiForbiddenResponse({
    description:
      'Insufficient available memory to upload the file OR User does not have sufficient rights or permissions for this operation',
  })
  @ApiConflictResponse({
    description: 'File with such a name already exists',
  })
  @ApiUnauthorizedResponse({
    description:
      'Invalid token or refreshToken or the user is not authenticated.',
  })
  @UseGuards(AuthGuard)
  @UseInterceptors(FileInterceptor('file'))
  async addFile(
    @UploadedFile(
      new ParseFilePipe({
        validators: [new MaxFileSizeValidator({ maxSize: 5000000 })],
      }),
    )
    file: Express.Multer.File,
    @Body() fileDto: CreateFileDto,
    @CurrentUser() user: User,
  ) {
    if (user.availableMemory - file.size < 0) {
      throw new ForbiddenException(
        'Insufficient available memory to upload the file',
      );
    }
    const rootFolder = new Types.ObjectId(fileDto.rootFolder);

    const foundRootFolder = await this.foldersService.findOne({
      _id: rootFolder,
    });
    if (!foundRootFolder) {
      throw new NotFoundException('Folder not found');
    }

    if (
      foundRootFolder.owner.toString() !== user._id.toString() &&
      !foundRootFolder.permissions.find(
        (permit) => permit.email === user.email && permit.role === 'editor',
      )
    ) {
      throw new ForbiddenException(
        'User does not have sufficient rights or permissions for this operation',
      );
    }
    const foundFile = await this.fileService.findOne({
      name: fileDto.name,
      owner: user._id,
      rootFolder,
    });
    if (foundFile) {
      throw new ConflictException('File with such a name already exists');
    }
    const fileId = new Types.ObjectId();

    const uploadedFile = await this.fileService.uploadRemoteFile(
      fileId.toString(),
      file.buffer,
    );

    const createdFile = await this.fileService.create({
      name: fileDto.name,
      owner: user._id,
      _id: fileId,
      isPrivate: fileDto.isPrivate,
      size: file.size,
      url: uploadedFile,
      rootFolder,
      ...(foundRootFolder && {
        permissions: foundRootFolder.permissions.filter(
          (permit) => permit.email !== user.email,
        ),
      }),
    });

    if (createdFile) {
      await this.foldersService.findOneAndUpdate(
        {
          _id: rootFolder,
          $or: [
            { owner: user._id },
            { 'permissions.email': user.email, 'permissions.role': 'editor' },
          ],
        },
        {
          $push: { files: createdFile._id },
        },
      );
    }

    return createdFile;
  }
}
