import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { Types } from 'mongoose';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users/users.service';
@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private readonly jwtService: JwtService,
    private readonly usersService: UsersService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const { authorization = '' } = request.headers;
    const [bearer, token] = authorization.split(' ');

    if (bearer !== 'Bearer' || !token) {
      this.handleAuthError('Invalid token');
    }

    try {
      const id = await this.verifyToken(token, process.env.SECRET_KEY);
      const userId = new Types.ObjectId(id);
      const foundUser = await this.usersService.findById(userId);
      if (!foundUser || foundUser.token !== token) {
        this.handleAuthError('Invalid token');
      }

      request.user = foundUser;

      return true;
    } catch (error) {
      if (error.name === 'UnauthorizedException') {
        await this.refreshToken(request);
        return true;
      } else {
        this.handleAuthError('Invalid token');
      }
    }
  }

  private async refreshToken(request): Promise<boolean> {
    const { authorization = '' } = request.headers;
    const [, refreshToken] = authorization.split(' ');

    if (!refreshToken) {
      this.handleAuthError('No refresh token provided');
    }
    try {
      const id = await this.verifyToken(
        refreshToken,
        process.env.REFRESH_SECRET_KEY,
      );
      const userId = new Types.ObjectId(id);
      const foundUser = await this.usersService.findById(userId);
      if (!foundUser || foundUser.refreshToken !== refreshToken) {
        this.handleAuthError('Invalid refresh token');
      }

      request.user = foundUser;

      return true;
    } catch (error) {
      this.handleAuthError();
    }
  }

  private async verifyToken(token, secret) {
    try {
      const { id } = await this.jwtService.verifyAsync(token, { secret });
      return id;
    } catch (error) {
      this.handleAuthError();
    }
  }

  private handleAuthError(message = 'Authentication failed'): never {
    throw new UnauthorizedException(message);
  }
}
