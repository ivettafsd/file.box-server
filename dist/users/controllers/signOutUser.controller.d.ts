import { User } from '../../schemas/User.schema';
import { AuthService } from '../auth.service';
export declare class SignOutUserController {
    private authService;
    constructor(authService: AuthService);
    signOut(user: User, res: any): Promise<void>;
}
