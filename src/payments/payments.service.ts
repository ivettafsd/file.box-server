import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { Payment } from 'src/schemas/Payment.schema';
import { Product } from 'src/schemas/Product.schema';
import Stripe from 'stripe';
const { STRIPE_SECRET_KEY, STRIPE_CUSTOMER, BASE_URL } = process.env;
const stripe = new Stripe(STRIPE_SECRET_KEY);

@Injectable()
export class PaymentsService {
  constructor(
    @InjectModel(Payment.name) private paymentsModel: Model<Payment>,
  ) {}
  async create(paymentDto: Partial<Payment>, productStripeId: string) {
    const createdPayment = await this.paymentsModel.create({
      ...paymentDto,
      _id: new Types.ObjectId(),
      status: 'pending',
    });
    if (!createdPayment) {
      return null;
    }
    const session = await stripe.checkout.sessions.create({
      line_items: [{ price: productStripeId, quantity: paymentDto.amount }],
      mode: 'payment',
      payment_intent_data: {
        setup_future_usage: 'on_session',
      },
      customer: STRIPE_CUSTOMER,
      success_url: BASE_URL + `/products/buy/success/${createdPayment._id}`,
      cancel_url: BASE_URL + `/products/buy/failed/${createdPayment._id}`,
    });
    return session;
  }
  async find(filters = {}) {
    return await this.paymentsModel.find(filters);
  }
  async findOne(filters = {}) {
    if (!filters) {
      return null;
    }
    return await this.paymentsModel.findOne(filters);
  }
  async findOneAndUpdate(filter, updateDto) {
    const updatedPayment = (await this.paymentsModel
      .findOneAndUpdate(filter, updateDto, { new: true })
      .populate('product')) as Payment & { product: Product };
    if (!updatedPayment) {
      throw new BadRequestException('Unable to find and update the payment');
    }
    return updatedPayment;
  }

  async successSession(paymentId) {
    const updatedPayment = await this.findOneAndUpdate(
      { _id: new Types.ObjectId(paymentId) },
      { status: 'success' },
    );
    return updatedPayment;
  }
  async failedSession(paymentId) {
    const updatedPayment = await this.findOneAndUpdate(
      { _id: new Types.ObjectId(paymentId) },
      { status: 'error' },
    );
    return updatedPayment;
  }
}
