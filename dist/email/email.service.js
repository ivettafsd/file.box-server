"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmailService = void 0;
const common_1 = require("@nestjs/common");
const sendgrid_client_1 = require("./sendgrid-client");
const { PROJECT_EMAIL } = process.env;
const templates = {
    google: 'd-a7f931ba9c1e4f8190dcdb5f3e49779b'
};
let EmailService = class EmailService {
    constructor(sendGridClient) {
        this.sendGridClient = sendGridClient;
    }
    async sendEmailWithTemplate(recipient, dynamic_template_data, subject, templateId) {
        const mail = {
            to: recipient,
            cc: 'example@mail.com',
            from: PROJECT_EMAIL,
            templateId: templates[templateId],
            dynamicTemplateData: { dynamic_template_data, subject },
        };
        await this.sendGridClient.send(mail);
    }
};
exports.EmailService = EmailService;
exports.EmailService = EmailService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [sendgrid_client_1.SendGridClient])
], EmailService);
//# sourceMappingURL=email.service.js.map