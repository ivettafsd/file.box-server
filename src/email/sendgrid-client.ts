import { Injectable, Logger } from '@nestjs/common';
import * as SendGrid from '@sendgrid/mail';

@Injectable()
export class SendGridClient {
  private readonly logger = new Logger(SendGridClient.name);

  constructor() {
    const apiKey = process.env.SENDGRID_API_KEY;
    if (!apiKey) {
      this.logger.error(
        'SendGrid API key is not set in environment variables.',
      );
      throw new Error('SendGrid API key is not set in environment variables.');
    }
    SendGrid.setApiKey(apiKey);
  }

  async send(mail: SendGrid.MailDataRequired): Promise<void> {
    try {
      await SendGrid.send(mail);
      this.logger.log(`Email successfully dispatched to ${mail.to as string}`);
    } catch (error) {
      this.logger.error(
        `Error while sending email to ${mail.to as string}`,
        error.stack,
      );
      throw error;
    }
  }
}
