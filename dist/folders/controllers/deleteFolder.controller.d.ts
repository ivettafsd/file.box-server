import { FoldersService } from './../folders.service';
import { User } from 'src/schemas/User.schema';
export declare class DeleteFolderController {
    private readonly foldersService;
    constructor(foldersService: FoldersService);
    removeFolder({ id }: {
        id: any;
    }, user: User, res: any): Promise<void>;
}
