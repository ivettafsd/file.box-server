import { AuthService } from './../auth.service';
export declare class AuthUsersWithGoogleCallbackController {
    private authService;
    constructor(authService: AuthService);
    googleAuthRedirect(req: any): Promise<{
        url: string;
    }>;
}
