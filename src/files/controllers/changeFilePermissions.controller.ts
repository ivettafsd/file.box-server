import {
  Controller,
  UseGuards,
  Body,
  Param,
  Patch,
  NotFoundException,
} from '@nestjs/common';
import { Types } from 'mongoose';
import { CurrentUser } from 'src/users/decorators/currentUser.decorator';
import { AuthGuard } from '../../guards/auth.guard';
import { User } from 'src/schemas/User.schema';
import { FilesService } from './../files.service';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiBody,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Permission } from './../dto/File.dto';
import { File } from 'src/schemas/File.schema';
import { ValidId } from '../decorators/validID.decorator';

@Controller('files')
@ApiTags('Files')
export class ChangeFilePermissionsController {
  constructor(private fileService: FilesService) {}

  @Patch('/:id/permissions')
  @ApiOperation({
    summary: 'Update permissions for a file',
  })
  @ApiBearerAuth()
  @ApiParam({
    name: 'id',
    description: 'File ID',
    example: '66471a9ce46805482dd323a4',
  })
  @ApiBody({
    description: 'Array containing the updated permissions for the file',
    type: [Permission],
  })
  @ApiOkResponse({
    description: 'Permissions updated successfully for the file',
    type: File,
  })
  @ApiNotFoundResponse({
    description: 'File not found or does not belong to the current user',
  })
  @ApiBadRequestResponse({
    description: 'Invalid file ID',
  })
  @ApiForbiddenResponse({
    description:
      'User does not have sufficient rights or permissions for this operation',
  })
  @ApiUnauthorizedResponse({
    description:
      'Invalid token or refreshToken or the user is not authenticated.',
  })
  @UseGuards(AuthGuard)
  async updateFilePermissions(
    @Param() @ValidId() { id },
    @Body() fileDto: [Permission],
    @CurrentUser() user: User,
  ) {
    const filter = { _id: new Types.ObjectId(id), owner: user._id };
    const updatedFile = await this.fileService.findOneAndUpdate(
      filter,
      fileDto,
    );
    if (!updatedFile) {
      throw new NotFoundException(
        'File not found or does not belong to the current user',
      );
    }
    return updatedFile;
  }
}
