import { Controller, Post, UseGuards, Res } from '@nestjs/common';
import { User } from '../../schemas/User.schema';
import { AuthGuard } from '../../guards/auth.guard';
import { CurrentUser } from '../decorators/currentUser.decorator';
import { AuthService } from '../auth.service';
import {
  ApiNotFoundResponse,
  ApiTags,
  ApiOkResponse,
  ApiBearerAuth,
  ApiOperation,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';

@Controller('users')
@ApiTags('Users')
export class SignOutUserController {
  constructor(private authService: AuthService) {}

  @Post('/signout')
  @ApiOperation({
    summary: 'Sign out the currently authenticated user',
  })
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Sign out success',
  })
  @ApiNotFoundResponse({
    description: 'User not found',
  })
  @ApiUnauthorizedResponse({
    description:
      'Invalid token or refreshToken or the user is not authenticated.',
  })
  @UseGuards(AuthGuard)
  async signOut(@CurrentUser() user: User, @Res() res) {
    await this.authService.signOut(user._id);
    res.status(200).json({ message: 'Sign out success' });
  }
}
