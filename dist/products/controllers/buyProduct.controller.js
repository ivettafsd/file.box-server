"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuyProductController = void 0;
const common_1 = require("@nestjs/common");
const auth_guard_1 = require("../../guards/auth.guard");
const currentUser_decorator_1 = require("../../users/decorators/currentUser.decorator");
const swagger_1 = require("@nestjs/swagger");
const User_schema_1 = require("../../schemas/User.schema");
const validID_decorator_1 = require("../../users/decorators/validID.decorator");
const products_service_1 = require("../products.service");
const mongoose_1 = require("mongoose");
const payments_service_1 = require("../../payments/payments.service");
let BuyProductController = class BuyProductController {
    constructor(productsService, paymentsService) {
        this.productsService = productsService;
        this.paymentsService = paymentsService;
    }
    async buyExtraMemory({ id }, user) {
        const foundProduct = await this.productsService.findOne({
            _id: new mongoose_1.Types.ObjectId(id),
        });
        if (!foundProduct) {
            throw new common_1.NotFoundException('Product not found');
        }
        const newPayment = {
            payer: user._id,
            amount: foundProduct.price,
            currency: foundProduct.currency,
            product: foundProduct._id,
        };
        const createdPayment = await this.paymentsService.create(newPayment, foundProduct.stripeID);
        if (!createdPayment) {
            throw new common_1.BadRequestException('Invalid data');
        }
        if (createdPayment && createdPayment.url) {
            return { url: createdPayment.url };
        }
        else {
            throw new common_1.BadRequestException('Invalid session data');
        }
    }
};
exports.BuyProductController = BuyProductController;
__decorate([
    (0, common_1.Post)('/buy/:id'),
    (0, swagger_1.ApiOperation)({
        summary: 'Purchase a product',
        description: 'Allows a user to purchase additional memory or other products by providing a product ID.',
    }),
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiParam)({
        name: 'id',
        description: 'ID of the product to be purchased',
        example: '66471a9ce46805482dd323a4',
    }),
    (0, swagger_1.ApiOkResponse)({
        description: 'Redirects the user to the payment URL',
    }),
    (0, swagger_1.ApiNotFoundResponse)({
        description: 'The product with the given ID was not found',
    }),
    (0, swagger_1.ApiBadRequestResponse)({
        description: 'Invalid data provided',
    }),
    (0, swagger_1.ApiUnauthorizedResponse)({
        description: 'The user is not authenticated',
    }),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    __param(0, (0, common_1.Param)()),
    __param(0, (0, validID_decorator_1.ValidId)()),
    __param(1, (0, currentUser_decorator_1.CurrentUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, User_schema_1.User]),
    __metadata("design:returntype", Promise)
], BuyProductController.prototype, "buyExtraMemory", null);
exports.BuyProductController = BuyProductController = __decorate([
    (0, common_1.Controller)('products'),
    (0, swagger_1.ApiTags)('Products'),
    __metadata("design:paramtypes", [products_service_1.ProductsService,
        payments_service_1.PaymentsService])
], BuyProductController);
//# sourceMappingURL=buyProduct.controller.js.map