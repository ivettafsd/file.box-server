/// <reference types="mongoose/types/aggregate" />
/// <reference types="mongoose/types/callback" />
/// <reference types="mongoose/types/collection" />
/// <reference types="mongoose/types/connection" />
/// <reference types="mongoose/types/cursor" />
/// <reference types="mongoose/types/document" />
/// <reference types="mongoose/types/error" />
/// <reference types="mongoose/types/expressions" />
/// <reference types="mongoose/types/helpers" />
/// <reference types="mongoose/types/middlewares" />
/// <reference types="mongoose/types/indexes" />
/// <reference types="mongoose/types/models" />
/// <reference types="mongoose/types/mongooseoptions" />
/// <reference types="mongoose/types/pipelinestage" />
/// <reference types="mongoose/types/populate" />
/// <reference types="mongoose/types/query" />
/// <reference types="mongoose/types/schemaoptions" />
/// <reference types="mongoose/types/schematypes" />
/// <reference types="mongoose/types/session" />
/// <reference types="mongoose/types/types" />
/// <reference types="mongoose/types/utility" />
/// <reference types="mongoose/types/validation" />
/// <reference types="mongoose/types/virtuals" />
/// <reference types="mongoose/types/inferschematype" />
import { FoldersService } from '../folders.service';
import { User } from 'src/schemas/User.schema';
import { Types } from 'mongoose';
import { FilesService } from 'src/files/files.service';
export declare class GetFolderByIDController {
    private readonly foldersService;
    private readonly filesService;
    constructor(foldersService: FoldersService, filesService: FilesService);
    getFolderById({ id }: {
        id: any;
    }, user: User): Promise<(import("mongoose").Document<unknown, {}, import("../../schemas/Folder.schema").Folder> & import("../../schemas/Folder.schema").Folder & Required<{
        _id: Types.ObjectId;
    }>) | {
        permissiveFolders: (import("mongoose").Document<unknown, {}, import("../../schemas/Folder.schema").Folder> & import("../../schemas/Folder.schema").Folder & Required<{
            _id: Types.ObjectId;
        }>)[];
        permissiveFiles: (import("mongoose").Document<unknown, {}, import("../../schemas/File.schema").File> & import("../../schemas/File.schema").File & Required<{
            _id: Types.ObjectId;
        }>)[];
        _id: Types.ObjectId;
        owner: Types.ObjectId;
        title: string;
        files: Types.ObjectId[];
        folders: Types.ObjectId[];
        rootFolder: Types.ObjectId;
        isPrivate: boolean;
        permissions: {
            role: string;
            email: string;
        }[];
        __v?: any;
        $locals: Record<string, unknown>;
        $op: "remove" | "save" | "validate";
        $where: Record<string, unknown>;
        baseModelName?: string;
        collection: import("mongoose").Collection<import("bson").Document>;
        db: import("mongoose").Connection;
        errors?: import("mongoose").Error.ValidationError;
        id?: any;
        isNew: boolean;
        schema: import("mongoose").Schema<any, import("mongoose").Model<any, any, any, any, any, any>, {}, {}, {}, {}, import("mongoose").DefaultSchemaOptions, {
            [x: string]: unknown;
        }, import("mongoose").Document<unknown, {}, import("mongoose").FlatRecord<{
            [x: string]: unknown;
        }>> & import("mongoose").FlatRecord<{
            [x: string]: unknown;
        }> & Required<{
            _id: unknown;
        }>>;
    }>;
}
