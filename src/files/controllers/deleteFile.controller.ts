import {
  Controller,
  UseGuards,
  Delete,
  Param,
  NotFoundException,
  Res,
} from '@nestjs/common';
import { Types } from 'mongoose';
import { CurrentUser } from 'src/users/decorators/currentUser.decorator';
import { AuthGuard } from '../../guards/auth.guard';
import { User } from 'src/schemas/User.schema';
import { FilesService } from './../files.service';
import { FoldersService } from 'src/folders/folders.service';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { ValidId } from '../decorators/validID.decorator';

@Controller('files')
@ApiTags('Files')
export class DeleteFileController {
  constructor(
    private fileService: FilesService,
    private foldersService: FoldersService,
  ) {}

  @Delete('/:id')
  @ApiOperation({
    summary: 'Delete a file by its ID',
  })
  @ApiBearerAuth()
  @ApiParam({
    name: 'id',
    description: 'File ID',
    example: '66471a9ce46805482dd323a4',
  })
  @ApiOkResponse({
    description: 'Removed file success',
  })
  @ApiNotFoundResponse({
    description: 'File not found or does not belong to the current user',
  })
  @ApiBadRequestResponse({
    description: 'Invalid file ID',
  })
  @ApiUnauthorizedResponse({
    description:
      'Invalid token or refreshToken or the user is not authenticated.',
  })
  @UseGuards(AuthGuard)
  async removeFile(
    @Param() @ValidId() { id },
    @CurrentUser() user: User,
    @Res() res,
  ) {
    const filter = { _id: new Types.ObjectId(id), owner: user._id };
    const deletedFile = await this.fileService.findOneAndDelete(filter);

    if (!deletedFile) {
      throw new NotFoundException(
        'File not found or does not belong to the current user',
      );
    }

    const rootFolder = new Types.ObjectId(deletedFile.rootFolder);

    await this.foldersService.findOneAndUpdate(
      { _id: rootFolder, owner: user._id },
      {
        $pull: { files: deletedFile._id },
      },
    );

    res.status(200).json({ message: 'Removed file success' });
  }
}
