import {
  BadRequestException,
  Controller,
  NotFoundException,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '../../guards/auth.guard';
import { CurrentUser } from '../../users/decorators/currentUser.decorator';
import {
  ApiNotFoundResponse,
  ApiTags,
  ApiUnauthorizedResponse,
  ApiBearerAuth,
  ApiParam,
  ApiBadRequestResponse,
  ApiOperation,
  ApiOkResponse,
} from '@nestjs/swagger';
import { User } from '../../schemas/User.schema';
import { ValidId } from '../../users/decorators/validID.decorator';
import { ProductsService } from 'src/products/products.service';
import { Types } from 'mongoose';
import { PaymentsService } from 'src/payments/payments.service';

@Controller('products')
@ApiTags('Products')
export class BuyProductController {
  constructor(
    private productsService: ProductsService,
    private paymentsService: PaymentsService,
  ) {}

  @Post('/buy/:id')
  @ApiOperation({
    summary: 'Purchase a product',
    description:
      'Allows a user to purchase additional memory or other products by providing a product ID.',
  })
  @ApiBearerAuth()
  @ApiParam({
    name: 'id',
    description: 'ID of the product to be purchased',
    example: '66471a9ce46805482dd323a4',
  })
  @ApiOkResponse({
    description: 'Redirects the user to the payment URL',
  })
  @ApiNotFoundResponse({
    description: 'The product with the given ID was not found',
  })
  @ApiBadRequestResponse({
    description: 'Invalid data provided',
  })
  @ApiUnauthorizedResponse({
    description: 'The user is not authenticated',
  })
  @UseGuards(AuthGuard)
  async buyExtraMemory(@Param() @ValidId() { id }, @CurrentUser() user: User) {
    const foundProduct = await this.productsService.findOne({
      _id: new Types.ObjectId(id),
    });
    if (!foundProduct) {
      throw new NotFoundException('Product not found');
    }
    const newPayment = {
      payer: user._id,
      amount: foundProduct.price,
      currency: foundProduct.currency,
      product: foundProduct._id,
    };
    const createdPayment = await this.paymentsService.create(
      newPayment,
      foundProduct.stripeID,
    );
    if (!createdPayment) {
      throw new BadRequestException('Invalid data');
    }

    if (createdPayment && createdPayment.url) {
      return { url: createdPayment.url };
    } else {
      throw new BadRequestException('Invalid session data');
    }
  }
}
