import { SendGridClient } from './sendgrid-client';
import { EmailDto } from './dto/Email.dto';
export declare class EmailService {
    private readonly sendGridClient;
    constructor(sendGridClient: SendGridClient);
    sendEmailWithTemplate(recipient: string, dynamic_template_data: EmailDto, subject: string, templateId: string): Promise<void>;
}
