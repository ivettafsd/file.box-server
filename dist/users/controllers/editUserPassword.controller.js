"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EditUserPasswordController = void 0;
const common_1 = require("@nestjs/common");
const User_schema_1 = require("../../schemas/User.schema");
const auth_guard_1 = require("../../guards/auth.guard");
const currentUser_decorator_1 = require("./../decorators/currentUser.decorator");
const auth_service_1 = require("./../auth.service");
const swagger_1 = require("@nestjs/swagger");
const User_dto_1 = require("../dto/User.dto");
let EditUserPasswordController = class EditUserPasswordController {
    constructor(authService) {
        this.authService = authService;
    }
    async changePassword(changePasswordDto, user) {
        const refreshedUser = await this.authService.changeUserPassword(changePasswordDto, user);
        return refreshedUser;
    }
};
exports.EditUserPasswordController = EditUserPasswordController;
__decorate([
    (0, common_1.Patch)('/edit/password'),
    (0, swagger_1.ApiOperation)({
        summary: 'Change user password',
    }),
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiOkResponse)({
        description: 'The password has been successfully changed. Returns the updated user object.',
        type: User_schema_1.User,
    }),
    (0, swagger_1.ApiUnauthorizedResponse)({
        description: 'Unauthorized. The provided token is invalid or expired, or the user is not authenticated.',
    }),
    (0, swagger_1.ApiNotFoundResponse)({
        description: 'User not found. The user does not exist in the database.',
    }),
    (0, swagger_1.ApiBadRequestResponse)({
        description: 'Email or password invalid',
    }),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, currentUser_decorator_1.CurrentUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [User_dto_1.ChangeUserPasswordDto,
        User_schema_1.User]),
    __metadata("design:returntype", Promise)
], EditUserPasswordController.prototype, "changePassword", null);
exports.EditUserPasswordController = EditUserPasswordController = __decorate([
    (0, common_1.Controller)('users'),
    (0, swagger_1.ApiTags)('Users'),
    __metadata("design:paramtypes", [auth_service_1.AuthService])
], EditUserPasswordController);
//# sourceMappingURL=editUserPassword.controller.js.map