"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WhoAmIController = void 0;
const common_1 = require("@nestjs/common");
const User_schema_1 = require("../../schemas/User.schema");
const auth_guard_1 = require("../../guards/auth.guard");
const currentUser_decorator_1 = require("./../decorators/currentUser.decorator");
const swagger_1 = require("@nestjs/swagger");
let WhoAmIController = class WhoAmIController {
    whoAmI(user) {
        return user;
    }
};
exports.WhoAmIController = WhoAmIController;
__decorate([
    (0, common_1.Get)('/whoami'),
    (0, swagger_1.ApiOperation)({
        summary: 'Get information about the currently authenticated user',
    }),
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiOkResponse)({
        description: 'Returns the currently authenticated user based on the provided authentication token.',
        type: User_schema_1.User,
    }),
    (0, swagger_1.ApiUnauthorizedResponse)({
        description: 'Invalid token or the user is not authenticated.',
    }),
    (0, swagger_1.ApiNotFoundResponse)({
        description: 'User not found',
    }),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    __param(0, (0, currentUser_decorator_1.CurrentUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [User_schema_1.User]),
    __metadata("design:returntype", void 0)
], WhoAmIController.prototype, "whoAmI", null);
exports.WhoAmIController = WhoAmIController = __decorate([
    (0, common_1.Controller)('users'),
    (0, swagger_1.ApiTags)('Users')
], WhoAmIController);
//# sourceMappingURL=whoAmI.controller.js.map