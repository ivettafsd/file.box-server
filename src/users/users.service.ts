import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { User } from '../schemas/User.schema';
@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private usersModel: Model<User>) {}

  async create(createDto) {
    const createdUser = await this.usersModel.create(createDto);
    const { password, ...user } = createdUser.toObject();
    return user;
  }

  async update(_id: Types.ObjectId, attrs) {
    const updatedUser = await this.usersModel.findByIdAndUpdate(_id, attrs, {
      new: true,
    });
    if (!updatedUser) {
      return null;
    }
    const { password, ...user } = updatedUser.toObject();
    return user;
  }
  async findById(_id: Types.ObjectId) {
    if (!_id) {
      return null;
    }
    const foundUser = await this.usersModel.findOne({ _id });
    const { password, ...user } = foundUser.toObject();
    return user;
  }
  async findOne(filters: Partial<User>) {
    if (!filters) {
      return null;
    }
    const foundUser = await this.usersModel.findOne(filters);
    return foundUser;
  }
}
