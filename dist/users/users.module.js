"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const User_schema_1 = require("../schemas/User.schema");
const users_service_1 = require("./users.service");
const auth_service_1 = require("./auth.service");
const jwt_1 = require("@nestjs/jwt");
const folders_service_1 = require("../folders/folders.service");
const Folder_schema_1 = require("../schemas/Folder.schema");
const File_schema_1 = require("../schemas/File.schema");
const files_service_1 = require("../files/files.service");
const email_module_1 = require("../email/email.module");
const signUpUser_controller_1 = require("./controllers/signUpUser.controller");
const signInUser_controller_1 = require("./controllers/signInUser.controller");
const authUserWithGoogle_controller_1 = require("./controllers/authUserWithGoogle.controller");
const authUserWithGoogleCallback_controll_1 = require("./controllers/authUserWithGoogleCallback.controll");
const whoAmI_controller_1 = require("./controllers/whoAmI.controller");
const refreshUser_controller_1 = require("./controllers/refreshUser.controller");
const signOutUser_controller_1 = require("./controllers/signOutUser.controller");
const editUserPassword_controller_1 = require("./controllers/editUserPassword.controller");
let UserModule = class UserModule {
};
exports.UserModule = UserModule;
exports.UserModule = UserModule = __decorate([
    (0, common_1.Module)({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                {
                    name: User_schema_1.User.name,
                    schema: User_schema_1.UserSchema,
                },
                {
                    name: Folder_schema_1.Folder.name,
                    schema: Folder_schema_1.FolderSchema,
                },
                {
                    name: File_schema_1.File.name,
                    schema: File_schema_1.FileSchema,
                },
            ]),
            email_module_1.EmailModule,
        ],
        controllers: [
            signUpUser_controller_1.SignUpUserController,
            signInUser_controller_1.SignInUserController,
            authUserWithGoogle_controller_1.AuthUserWithGoogleController,
            authUserWithGoogleCallback_controll_1.AuthUsersWithGoogleCallbackController,
            whoAmI_controller_1.WhoAmIController,
            refreshUser_controller_1.RefreshUserController,
            signOutUser_controller_1.SignOutUserController,
            editUserPassword_controller_1.EditUserPasswordController,
        ],
        providers: [
            users_service_1.UsersService,
            auth_service_1.AuthService,
            jwt_1.JwtService,
            folders_service_1.FoldersService,
            files_service_1.FilesService,
        ],
    })
], UserModule);
//# sourceMappingURL=users.module.js.map