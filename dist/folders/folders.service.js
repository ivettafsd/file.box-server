"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FoldersService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const Folder_schema_1 = require("../schemas/Folder.schema");
const File_schema_1 = require("../schemas/File.schema");
const files_service_1 = require("../files/files.service");
let FoldersService = class FoldersService {
    constructor(foldersModel, filesModel, filesService) {
        this.foldersModel = foldersModel;
        this.filesModel = filesModel;
        this.filesService = filesService;
    }
    async create(createDto) {
        const createdFolder = await this.foldersModel.create(createDto);
        return createdFolder;
    }
    async findOneAndUpdate(filter, updateDto) {
        const updatedFolder = await this.foldersModel.findOneAndUpdate(filter, updateDto, { new: true });
        if (!updatedFolder) {
            throw new common_1.ForbiddenException('Folder not found or does not belong to the current user');
        }
        return updatedFolder;
    }
    async findOneAndDelete(filter) {
        const foundFolder = await this.findOne(filter);
        if (!foundFolder) {
            throw new common_1.ForbiddenException('Folder not found or does not belong to the current user');
        }
        await this.foldersModel.findOneAndDelete(filter);
        await this.deleteSubfolders(foundFolder.folders);
        await this.filesService.deleteSubfiles(foundFolder.files);
        await this.foldersModel.findOneAndUpdate({ _id: foundFolder.rootFolder, owner: foundFolder.owner }, { $pull: { folders: foundFolder._id } });
        return foundFolder;
    }
    async find(filters) {
        return await this.foldersModel
            .find(filters)
            .populate('folders', '', this.foldersModel)
            .populate('files', '', this.filesModel);
    }
    async deleteSubfolders(folderIds) {
        for (const folder of folderIds) {
            await this.findOneAndDelete({ _id: folder._id });
        }
    }
    async updateSubfolders(folderIds, updateDto) {
        for (const folder of folderIds) {
            await this.findOneAndUpdate({ _id: folder._id }, updateDto);
        }
    }
    async findOne(filters) {
        if (!filters) {
            return null;
        }
        return await this.foldersModel
            .findOne(filters)
            .populate('folders', '', this.foldersModel)
            .populate('files', '', this.filesModel);
    }
};
exports.FoldersService = FoldersService;
exports.FoldersService = FoldersService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, mongoose_1.InjectModel)(Folder_schema_1.Folder.name)),
    __param(1, (0, mongoose_1.InjectModel)(File_schema_1.File.name)),
    __metadata("design:paramtypes", [mongoose_2.Model,
        mongoose_2.Model,
        files_service_1.FilesService])
], FoldersService);
//# sourceMappingURL=folders.service.js.map