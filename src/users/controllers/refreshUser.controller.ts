import { Controller, Get, UseGuards } from '@nestjs/common';
import { User } from '../../schemas/User.schema';
import { AuthGuard } from '../../guards/auth.guard';
import { CurrentUser } from './../decorators/currentUser.decorator';
import { AuthService } from './../auth.service';
import {
  ApiNotFoundResponse,
  ApiTags,
  ApiOkResponse,
  ApiUnauthorizedResponse,
  ApiBearerAuth,
  ApiOperation,
} from '@nestjs/swagger';

@Controller('users')
@ApiTags('Users')
export class RefreshUserController {
  constructor(private authService: AuthService) {}

  @Get('/refresh')
  @ApiOperation({
    summary: 'Refresh the authentication token',
  })
  @ApiBearerAuth()
  @ApiOkResponse({
    description:
      'Returns the currently authenticated user based on the provided authentication token or refresh token.',
    type: User,
  })
  @ApiUnauthorizedResponse({
    description:
      'Invalid token or refreshToken or the user is not authenticated.',
  })
  @ApiNotFoundResponse({
    description: 'User not found',
  })
  @UseGuards(AuthGuard)
  async refreshToken(@CurrentUser() user: User) {
    const refreshedUser = await this.authService.refreshTokens(user._id);
    return refreshedUser;
  }
}
