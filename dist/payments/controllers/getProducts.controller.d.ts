import { ProductsService } from '../payments.service';
export declare class GetProductsController {
    private readonly productsService;
    constructor(productsService: ProductsService);
    getProducts(): Promise<any>;
}
