/// <reference types="node" />
/// <reference types="mongoose/types/aggregate" />
/// <reference types="mongoose/types/callback" />
/// <reference types="mongoose/types/collection" />
/// <reference types="mongoose/types/connection" />
/// <reference types="mongoose/types/cursor" />
/// <reference types="mongoose/types/document" />
/// <reference types="mongoose/types/error" />
/// <reference types="mongoose/types/expressions" />
/// <reference types="mongoose/types/helpers" />
/// <reference types="mongoose/types/middlewares" />
/// <reference types="mongoose/types/indexes" />
/// <reference types="mongoose/types/models" />
/// <reference types="mongoose/types/mongooseoptions" />
/// <reference types="mongoose/types/pipelinestage" />
/// <reference types="mongoose/types/populate" />
/// <reference types="mongoose/types/query" />
/// <reference types="mongoose/types/schemaoptions" />
/// <reference types="mongoose/types/schematypes" />
/// <reference types="mongoose/types/session" />
/// <reference types="mongoose/types/types" />
/// <reference types="mongoose/types/utility" />
/// <reference types="mongoose/types/validation" />
/// <reference types="mongoose/types/virtuals" />
/// <reference types="mongoose/types/inferschematype" />
import { ConfigService } from '@nestjs/config';
import { Model, Types } from 'mongoose';
import { File } from '../schemas/File.schema';
import { UsersService } from 'src/users/users.service';
export declare class FilesService {
    private readonly configService;
    private filesModel;
    private readonly usersService;
    private readonly s3Client;
    constructor(configService: ConfigService, filesModel: Model<File>, usersService: UsersService);
    uploadRemoteFile(fileName: string, file: Buffer): Promise<string>;
    deleteRemoteFile(fileName: string): Promise<import("@aws-sdk/client-s3").DeleteObjectCommandOutput>;
    create(createDto: any): Promise<import("mongoose").Document<unknown, {}, File> & File & Required<{
        _id: Types.ObjectId;
    }>>;
    findOneAndUpdate(filter: any, updateDto: any): Promise<import("mongoose").Document<unknown, {}, File> & File & Required<{
        _id: Types.ObjectId;
    }>>;
    findOneAndDelete(filter: any): Promise<import("mongoose").Document<unknown, {}, File> & File & Required<{
        _id: Types.ObjectId;
    }>>;
    deleteSubfiles(fileIds: Types.ObjectId[]): Promise<void>;
    updateSubfiles(fileIds: Types.ObjectId[], updateDto: any): Promise<void>;
    find(filters: {}): Promise<(import("mongoose").Document<unknown, {}, File> & File & Required<{
        _id: Types.ObjectId;
    }>)[]>;
    findOne(filters: any): Promise<import("mongoose").Document<unknown, {}, File> & File & Required<{
        _id: Types.ObjectId;
    }>>;
}
