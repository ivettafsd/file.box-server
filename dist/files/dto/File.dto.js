"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FilesResponseDto = exports.FileDto = exports.CreateFileDto = exports.EditFileDto = exports.Permission = void 0;
const class_validator_1 = require("class-validator");
const class_transformer_1 = require("class-transformer");
const swagger_1 = require("@nestjs/swagger");
const Folder_schema_1 = require("../../schemas/Folder.schema");
const File_schema_1 = require("../../schemas/File.schema");
class Permission {
}
exports.Permission = Permission;
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'Role of the permission (viewer or editor)',
        example: 'editor',
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsIn)(['viewer', 'editor']),
    __metadata("design:type", String)
], Permission.prototype, "role", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'Email associated with the permission',
        example: 'user@example.com',
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsEmail)(),
    __metadata("design:type", String)
], Permission.prototype, "email", void 0);
class EditFileDto {
}
exports.EditFileDto = EditFileDto;
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'File name',
        example: 'My file',
    }),
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], EditFileDto.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({
        description: 'Indicates whether the file is private',
        example: true,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsBoolean)(),
    (0, class_transformer_1.Transform)((value) => Boolean(value)),
    __metadata("design:type", Boolean)
], EditFileDto.prototype, "isPrivate", void 0);
class CreateFileDto extends EditFileDto {
}
exports.CreateFileDto = CreateFileDto;
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'Root folder containing this file',
        example: '66471a9ce46805482dd323a4',
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], CreateFileDto.prototype, "rootFolder", void 0);
class FileDto extends CreateFileDto {
}
exports.FileDto = FileDto;
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'Permissions granted to users for accessing the file',
        example: [{ role: 'viewer', email: 'user@example.com' }],
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsArray)(),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => Permission),
    __metadata("design:type", Array)
], FileDto.prototype, "permissions", void 0);
class FilesResponseDto {
}
exports.FilesResponseDto = FilesResponseDto;
__decorate([
    (0, swagger_1.ApiProperty)({ type: [Folder_schema_1.Folder] }),
    __metadata("design:type", Array)
], FilesResponseDto.prototype, "folders", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [File_schema_1.File] }),
    __metadata("design:type", Array)
], FilesResponseDto.prototype, "files", void 0);
//# sourceMappingURL=File.dto.js.map