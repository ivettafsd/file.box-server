# File.box-server

### File.box-server is a backend service designed to manage file storage with features similar to Google Drive. This service supports user authentication via Google, file and folder hierarchy, permissions management, and sharing capabilities. It is built using Nest.js and offers a RESTful API documented with Swagger.

## Features
- **User Authentication:** Supports login via Google.
- **File and Folder Management:** Create, rename, delete, and move files and folders.
- **Permissions Management:** Set files and folders as public or private, and share access via email with specific permissions.
- **File Upload:** Users can upload files to the server.
- **Search Functionality:** Search for files and folders by keyword.
- **File Viewing:** Access and view files shared by other users.
- **API Documentation:** REST API documentation provided via Swagger.
- **Stripe Integration:** Payment gateway for purchasing extra memory for saving files.

## Technologies
- **Nest.js:** A progressive Node.js framework for building efficient, reliable, and scalable server-side applications.
- **TypeScript:** Typed JavaScript at any scale.
- **Swagger:** API documentation.
- **MongoDB:** Database for storing file metadata and user information.
- **AWS S3:** Storage service for file contents.
- **SendGrid:** Email service for sharing file access.
- **Stripe:** Payment processing for purchasing extra memory.

## Getting Started
### Prerequisites
- **Node.js:** Version 14.x or higher
- **MongoDB:** Local or remote instance
- **Google API credentials:** For Google OAuth
- **AWS S3 bucket:** For file storage
- **SendGrid account:** For email notifications
- **Stripe account:** For payment processing

## Installation

**Clone the repository:**
```sh
git clone https://gitlab.com/ivettafsd/file.box-server.git
cd file.box-server
```

**Install dependencies:**
```bash
$ npm install
```

**Set up environment variables:**
```plaintext
MONGO_DB=
SECRET_KEY=
REFRESH_SECRET_KEY=
AWS_S3_REGION=
AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_S3_BUCKET=
PORT=
GOOGLE_CLIENT_ID=
GOOGLE_CLIENT_SECRET=
BASE_URL=
FRONT_BASE_URL=
SENDGRID_API_KEY=
PROJECT_EMAIL=
PROJECT=
STRIPE_CUSTOMER=
STRIPE_PUBLISH_KEY=
STRIPE_SECRET_KEY=
```

## Running the app
```bash
# development
$ npm run start:dev

# production mode
$ npm run build
$ npm run start:prod
```

## API Documentation
API documentation is available via [Swagger](https://file-box-server.onrender.com/api)