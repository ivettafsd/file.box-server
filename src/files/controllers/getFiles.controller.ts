import { Controller, UseGuards, Get, Query } from '@nestjs/common';
import { CurrentUser } from 'src/users/decorators/currentUser.decorator';
import { AuthGuard } from '../../guards/auth.guard';
import { User } from 'src/schemas/User.schema';
import { FilesService } from '../files.service';
import { FilesResponseDto } from '../dto/File.dto';
import { FoldersService } from 'src/folders/folders.service';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiQuery,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';

@Controller('files')
@ApiTags('Files')
export class GetFilesController {
  constructor(
    private fileService: FilesService,
    private foldersService: FoldersService,
  ) {}

  @Get('/')
  @ApiOperation({
    summary:
      'Get all folders and files by keyword owned by the user and folders where the current user has permissions',
  })
  @ApiBearerAuth()
  @ApiQuery({
    name: 'keyword',
    description: 'Keyword',
    example: 'File',
    required: false,
  })
  @ApiOkResponse({
    description:
      'List of folders and files retrieved based on the provided keyword.',
    type: FilesResponseDto,
  })
  @ApiUnauthorizedResponse({
    description:
      'Invalid token or refreshToken or the user is not authenticated.',
  })
  @UseGuards(AuthGuard)
  async getFiles(
    @Query('keyword') keyword: string = '',
    @CurrentUser() user: User,
  ) {
    const filter = {
      $or: [
        { owner: user._id },
        { 'permissions.email': user.email, 'permissions.role': 'editor' },
      ],
    };
    const regexSearch = { $regex: keyword, $options: 'i' };
    const foundFolders = await this.foldersService.find({
      ...filter,
      title: regexSearch,
    });
    const foundFiles = await this.fileService.find({
      ...filter,
      name: regexSearch,
    });

    return {
      folders: foundFolders,
      files: foundFiles,
    };
  }
}
