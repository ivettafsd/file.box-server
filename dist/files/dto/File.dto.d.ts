import { Folder } from 'src/schemas/Folder.schema';
import { File } from 'src/schemas/File.schema';
export declare class Permission {
    role: string;
    email: string;
}
export declare class EditFileDto {
    name: string;
    isPrivate?: boolean;
}
export declare class CreateFileDto extends EditFileDto {
    rootFolder: string;
}
export declare class FileDto extends CreateFileDto {
    permissions: Permission[];
}
export declare class FilesResponseDto {
    folders: Folder[];
    files: File[];
}
