import { User } from 'src/schemas/User.schema';
import { FilesService } from './../files.service';
import { FoldersService } from 'src/folders/folders.service';
export declare class DeleteFileController {
    private fileService;
    private foldersService;
    constructor(fileService: FilesService, foldersService: FoldersService);
    removeFile({ id }: {
        id: any;
    }, user: User, res: any): Promise<void>;
}
