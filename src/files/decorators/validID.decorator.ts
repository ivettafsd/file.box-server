import {
  BadRequestException,
  createParamDecorator,
  ExecutionContext,
} from '@nestjs/common';
import { isValidObjectId } from 'mongoose';

export const ValidId = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const id = request.params.id;
    if (!isValidObjectId(id)) {
      throw new BadRequestException('Invalid ID');
    }
    return id;
  },
);
