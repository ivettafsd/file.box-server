"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateFolderController = void 0;
const common_1 = require("@nestjs/common");
const Folder_dto_1 = require("./../dto/Folder.dto");
const auth_guard_1 = require("../../guards/auth.guard");
const folders_service_1 = require("./../folders.service");
const currentUser_decorator_1 = require("../../users/decorators/currentUser.decorator");
const User_schema_1 = require("../../schemas/User.schema");
const mongoose_1 = require("mongoose");
const swagger_1 = require("@nestjs/swagger");
const Folder_schema_1 = require("../../schemas/Folder.schema");
let CreateFolderController = class CreateFolderController {
    constructor(foldersService) {
        this.foldersService = foldersService;
    }
    async createFolder(folderDto, user) {
        const rootFolder = new mongoose_1.Types.ObjectId(folderDto.rootFolder);
        const foundRootFolder = await this.foldersService.findOne({
            _id: rootFolder,
        });
        if (!foundRootFolder) {
            throw new common_1.NotFoundException('Folder not found');
        }
        if (foundRootFolder.owner.toString() !== user._id.toString() &&
            !foundRootFolder.permissions.find((permit) => permit.email === user.email && permit.role === 'editor')) {
            throw new common_1.ForbiddenException('User does not have sufficient rights or permissions for this operation');
        }
        const foundFolder = await this.foldersService.findOne({
            title: folderDto.title,
            rootFolder: new mongoose_1.Types.ObjectId(folderDto.rootFolder),
        });
        if (foundFolder) {
            throw new common_1.ConflictException('Such a folder already exists');
        }
        const createdFolder = await this.foldersService.create({
            title: folderDto.title,
            owner: foundRootFolder.owner,
            _id: new mongoose_1.Types.ObjectId(),
            isPrivate: foundRootFolder.owner.toString() !== user._id.toString()
                ? foundRootFolder.isPrivate
                : folderDto.isPrivate,
            rootFolder: new mongoose_1.Types.ObjectId(folderDto.rootFolder),
            permissions: foundRootFolder.permissions,
        });
        if (createdFolder) {
            await this.foldersService.findOneAndUpdate({
                _id: new mongoose_1.Types.ObjectId(folderDto.rootFolder),
            }, {
                $push: { folders: createdFolder._id },
            });
        }
        return createdFolder;
    }
};
exports.CreateFolderController = CreateFolderController;
__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiOperation)({ summary: 'Create a new folder' }),
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiCreatedResponse)({
        description: 'Folder successfully created',
        type: Folder_schema_1.Folder,
    }),
    (0, swagger_1.ApiNotFoundResponse)({
        description: 'Folder not found',
    }),
    (0, swagger_1.ApiConflictResponse)({
        description: 'Folder with such a title already exists in this folder',
    }),
    (0, swagger_1.ApiForbiddenResponse)({
        description: 'User does not have sufficient rights or permissions for this operation',
    }),
    (0, swagger_1.ApiUnauthorizedResponse)({
        description: 'Invalid token or refreshToken or the user is not authenticated.',
    }),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, currentUser_decorator_1.CurrentUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Folder_dto_1.CreateFolderDto,
        User_schema_1.User]),
    __metadata("design:returntype", Promise)
], CreateFolderController.prototype, "createFolder", null);
exports.CreateFolderController = CreateFolderController = __decorate([
    (0, common_1.Controller)('folders'),
    (0, swagger_1.ApiTags)('Folders'),
    __metadata("design:paramtypes", [folders_service_1.FoldersService])
], CreateFolderController);
//# sourceMappingURL=createFolder.controller.js.map