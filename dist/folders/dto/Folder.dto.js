"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FolderByIDResponseDto = exports.FoldersResponseDto = exports.FolderDto = exports.CreateFolderDto = exports.EditFolderDto = exports.Permission = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const File_schema_1 = require("../../schemas/File.schema");
const Folder_schema_1 = require("../../schemas/Folder.schema");
class Permission {
}
exports.Permission = Permission;
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'Role of the permission (viewer or editor)',
        example: 'editor',
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsIn)(['viewer', 'editor']),
    __metadata("design:type", String)
], Permission.prototype, "role", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'Email associated with the permission',
        example: 'user@example.com',
    }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsEmail)(),
    __metadata("design:type", String)
], Permission.prototype, "email", void 0);
class EditFolderDto {
}
exports.EditFolderDto = EditFolderDto;
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'Folder title',
        example: 'My folder',
    }),
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], EditFolderDto.prototype, "title", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({
        description: 'Indicates whether the folder is private',
        example: true,
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsBoolean)(),
    (0, class_transformer_1.Transform)((value) => Boolean(value)),
    __metadata("design:type", Boolean)
], EditFolderDto.prototype, "isPrivate", void 0);
class CreateFolderDto extends EditFolderDto {
}
exports.CreateFolderDto = CreateFolderDto;
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'Root folder containing this folder',
        example: '66471a9ce46805482dd323a4',
    }),
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], CreateFolderDto.prototype, "rootFolder", void 0);
class FolderDto extends CreateFolderDto {
}
exports.FolderDto = FolderDto;
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'Permissions granted to users for accessing the folder',
        example: [{ role: 'viewer', email: 'user@example.com' }],
    }),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsArray)(),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_transformer_1.Type)(() => Permission),
    __metadata("design:type", Array)
], FolderDto.prototype, "permissions", void 0);
class FoldersResponseDto {
}
exports.FoldersResponseDto = FoldersResponseDto;
__decorate([
    (0, swagger_1.ApiProperty)({ type: [Folder_schema_1.Folder] }),
    __metadata("design:type", Array)
], FoldersResponseDto.prototype, "folders", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [Folder_schema_1.Folder] }),
    __metadata("design:type", Array)
], FoldersResponseDto.prototype, "permissiveFolders", void 0);
class FolderByIDResponseDto extends (0, swagger_1.OmitType)(Folder_schema_1.Folder, [
    'files',
    'folders',
]) {
}
exports.FolderByIDResponseDto = FolderByIDResponseDto;
__decorate([
    (0, swagger_1.ApiProperty)({ type: [Folder_schema_1.Folder], description: 'Array of folder objects' }),
    __metadata("design:type", Array)
], FolderByIDResponseDto.prototype, "folders", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [File_schema_1.File], description: 'Array of file objects' }),
    __metadata("design:type", Array)
], FolderByIDResponseDto.prototype, "files", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({
        type: [Folder_schema_1.Folder],
        description: 'Array of permissive folder objects',
    }),
    __metadata("design:type", Array)
], FolderByIDResponseDto.prototype, "permissiveFolders", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({
        type: [File_schema_1.File],
        description: 'Array of permissive file objects',
    }),
    __metadata("design:type", Array)
], FolderByIDResponseDto.prototype, "permissiveFiles", void 0);
//# sourceMappingURL=Folder.dto.js.map