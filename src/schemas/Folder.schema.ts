import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document, Types } from 'mongoose';

class Permission {
  @Prop({ required: true, enum: ['viewer', 'editor'] })
  role: string;

  @Prop({ required: true })
  email: string;
}

@Schema({ versionKey: false, timestamps: true })
export class Folder extends Document {
  @ApiProperty({
    description: 'Folder ID',
    example: '66471a9ce46805482dd323a4',
  })
  @Prop({ required: true })
  _id: Types.ObjectId;

  @ApiProperty({
    description: 'Owner of the folder (User ID)',
    example: '66471a9ce46805482dd323a4',
  })
  @Prop({ type: Types.ObjectId, ref: 'User', required: true })
  owner: Types.ObjectId;

  @ApiProperty({
    description: 'Folder title',
    example: 'My folder',
  })
  @Prop({ required: true })
  title: string;

  @ApiProperty({
    description: 'Files contained in the folder',
    example: ['66471a9ce46805482dd323a4', '66471a9ce46805482dd323a5'],
  })
  @Prop({ type: [{ type: Types.ObjectId, ref: 'File' }], default: [] })
  files: Types.ObjectId[];

  @ApiProperty({
    description: 'Folders contained in the folder',
    example: ['66471a9ce46805482dd323a4', '66471a9ce46805482dd323a5'],
  })
  @Prop({ type: [{ type: Types.ObjectId, ref: 'this' }], default: [] })
  folders: Types.ObjectId[];

  @ApiProperty({
    description: 'Root folder containing this folder',
    example: '66471a9ce46805482dd323a4',
  })
  @Prop({ type: Types.ObjectId, ref: 'this', required: false })
  rootFolder: Types.ObjectId;

  @ApiProperty({
    description: 'Indicates whether the folder is private',
    example: true,
  })
  @Prop({ required: true, default: true })
  isPrivate: boolean;

  @ApiProperty({
    description: 'Permissions granted to users for accessing the folder',
    example: [{ role: 'viewer', email: 'user@example.com' }],
  })
  @Prop([Permission])
  permissions: Permission[] = [];
}

export const FolderSchema = SchemaFactory.createForClass(Folder);
