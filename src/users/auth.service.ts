import {
  BadRequestException,
  Injectable,
  NotFoundException,
  ConflictException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { EmailService } from '../email/email.service';
import {
  SignUpUserDto,
  SignInUserDto,
  ChangeUserPasswordDto,
} from './dto/User.dto';
import * as bcrypt from 'bcrypt';
import { Types } from 'mongoose';
import { UsersService } from './users.service';
import { FoldersService } from 'src/folders/folders.service';
const { SECRET_KEY, REFRESH_SECRET_KEY, PROJECT_EMAIL, PROJECT } = process.env;

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private foldersService: FoldersService,
    private jwtService: JwtService,
    private readonly emailService: EmailService,
  ) {}

  async signUp(createDto: SignUpUserDto) {
    const user = await this.usersService.findOne({
      email: createDto.email,
    });
    if (user) {
      throw new ConflictException('Email is already in use');
    }
    const hashPassword = await bcrypt.hash(createDto.password, 10);
    const userId = new Types.ObjectId();
    const createdUser = await this.usersService.create({
      email: createDto.email,
      password: hashPassword,
      name: createDto.name || createDto.email.split('@')[0],
      ...(createDto.avatar && { avatar: createDto.avatar }),
      _id: userId,
    });
    const mainFolder = await this.foldersService.create({
      title: 'Main folder',
      owner: createdUser._id,
      _id: createdUser._id,
    });
    const updatedUser = await this.usersService.update(createdUser._id, {
      mainFolder: mainFolder._id,
    });
    return updatedUser;
  }

  async signIn(createDto: SignInUserDto) {
    const user = await this.usersService.findOne({
      email: createDto.email,
    });
    if (!user) {
      throw new NotFoundException('User with such an email not found');
    }
    const isCorrectPassword = await bcrypt.compare(
      createDto.password,
      user.password,
    );
    if (!isCorrectPassword) {
      throw new BadRequestException('Email or password invalid');
    }
    const refreshedUser = await this.refreshTokens(user._id);
    return refreshedUser;
  }
  async changeUserPassword(changePasswordDto: ChangeUserPasswordDto, user) {
    const foundUser = await this.usersService.findOne({
      _id: user._id,
    });
    const isCorrectPassword = await bcrypt.compare(
      changePasswordDto.oldPassword,
      foundUser.password,
    );
    if (!isCorrectPassword) {
      throw new BadRequestException('Email or password invalid');
    }
    const hashPassword = await bcrypt.hash(changePasswordDto.newPassword, 10);

    const updatedUser = await this.usersService.update(user._id, {
      password: hashPassword,
    });
    return updatedUser;
  }
  async refreshTokens(userId) {
    const payload = { id: userId };
    const token = await this.jwtService.signAsync(payload, {
      secret: SECRET_KEY,
      expiresIn: '1h',
    });
    const refreshToken = await this.jwtService.signAsync(payload, {
      secret: REFRESH_SECRET_KEY,
      expiresIn: '7d',
    });
    const updatedUser = await this.usersService.update(userId, {
      token,
      refreshToken,
    });

    return updatedUser;
  }
  async signOut(userId) {
    const updatedUser = await this.usersService.update(userId, {
      token: null,
      refreshToken: null,
    });
    return updatedUser;
  }
  async googleLogin(req) {
    if (!req.user) {
      return null;
    }

    const foundUser = await this.usersService.findOne({
      email: req.user.email,
    });

    let userId;
    if (!foundUser) {
      const newPassword = new Types.ObjectId().toString();
      const registeredUser = await this.signUp({
        email: req.user.email,
        name: req.user.name,
        avatar: req.user.avatar,
        password: newPassword,
      });
      await this.emailService.sendEmailWithTemplate(
        req.user.email,
        {
          email: req.user.email,
          password: newPassword,
          name: req.user.name,
          project: PROJECT,
          support: PROJECT_EMAIL,
        },
        'You are signed in to File.box with Google',
        'google',
      );

      userId = registeredUser._id;
    } else {
      userId = foundUser._id;
    }

    const authorizedUser = await this.refreshTokens(userId);

    return authorizedUser;
  }
}
