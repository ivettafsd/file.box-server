"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChangeFilePermissionsController = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const currentUser_decorator_1 = require("../../users/decorators/currentUser.decorator");
const auth_guard_1 = require("../../guards/auth.guard");
const User_schema_1 = require("../../schemas/User.schema");
const files_service_1 = require("./../files.service");
const swagger_1 = require("@nestjs/swagger");
const File_dto_1 = require("./../dto/File.dto");
const File_schema_1 = require("../../schemas/File.schema");
const validID_decorator_1 = require("../decorators/validID.decorator");
let ChangeFilePermissionsController = class ChangeFilePermissionsController {
    constructor(fileService) {
        this.fileService = fileService;
    }
    async updateFilePermissions({ id }, fileDto, user) {
        const filter = { _id: new mongoose_1.Types.ObjectId(id), owner: user._id };
        const updatedFile = await this.fileService.findOneAndUpdate(filter, fileDto);
        if (!updatedFile) {
            throw new common_1.NotFoundException('File not found or does not belong to the current user');
        }
        return updatedFile;
    }
};
exports.ChangeFilePermissionsController = ChangeFilePermissionsController;
__decorate([
    (0, common_1.Patch)('/:id/permissions'),
    (0, swagger_1.ApiOperation)({
        summary: 'Update permissions for a file',
    }),
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiParam)({
        name: 'id',
        description: 'File ID',
        example: '66471a9ce46805482dd323a4',
    }),
    (0, swagger_1.ApiBody)({
        description: 'Array containing the updated permissions for the file',
        type: [File_dto_1.Permission],
    }),
    (0, swagger_1.ApiOkResponse)({
        description: 'Permissions updated successfully for the file',
        type: File_schema_1.File,
    }),
    (0, swagger_1.ApiNotFoundResponse)({
        description: 'File not found or does not belong to the current user',
    }),
    (0, swagger_1.ApiBadRequestResponse)({
        description: 'Invalid file ID',
    }),
    (0, swagger_1.ApiForbiddenResponse)({
        description: 'User does not have sufficient rights or permissions for this operation',
    }),
    (0, swagger_1.ApiUnauthorizedResponse)({
        description: 'Invalid token or refreshToken or the user is not authenticated.',
    }),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    __param(0, (0, common_1.Param)()),
    __param(0, (0, validID_decorator_1.ValidId)()),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, currentUser_decorator_1.CurrentUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Array, User_schema_1.User]),
    __metadata("design:returntype", Promise)
], ChangeFilePermissionsController.prototype, "updateFilePermissions", null);
exports.ChangeFilePermissionsController = ChangeFilePermissionsController = __decorate([
    (0, common_1.Controller)('files'),
    (0, swagger_1.ApiTags)('Files'),
    __metadata("design:paramtypes", [files_service_1.FilesService])
], ChangeFilePermissionsController);
//# sourceMappingURL=changeFilePermissions.controller.js.map