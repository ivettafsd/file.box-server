"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuyExtraMemoryController = void 0;
const common_1 = require("@nestjs/common");
const auth_guard_1 = require("../../guards/auth.guard");
const currentUser_decorator_1 = require("./../decorators/currentUser.decorator");
const swagger_1 = require("@nestjs/swagger");
const User_schema_1 = require("../../schemas/User.schema");
const users_service_1 = require("../users.service");
const validID_decorator_1 = require("../decorators/validID.decorator");
const products_service_1 = require("../../products/products.service");
const mongoose_1 = require("mongoose");
const User_dto_1 = require("../dto/User.dto");
let BuyExtraMemoryController = class BuyExtraMemoryController {
    constructor(usersService, productsService) {
        this.usersService = usersService;
        this.productsService = productsService;
    }
    async buyExtraMemory(createBuyMemoryDto, user) {
        const foundProduct = await this.productsService.findOne({
            _id: new mongoose_1.Types.ObjectId(createBuyMemoryDto.productId),
        });
        if (!foundProduct) {
            throw new common_1.NotFoundException('Product not found');
        }
        const session = await this.usersService.buyExtraMemory(foundProduct, user);
        if (session && session.url) {
            console.log('session.url', session.url);
            return { url: session.url };
        }
        else {
            throw new common_1.BadRequestException('Invalid session data');
        }
    }
    async paymentSuccess({ id }, res) {
        return this.usersService.successSession(res, id);
    }
    async paymentFailed({ id }, res) {
        return this.usersService.failedSession(res, id);
    }
};
exports.BuyExtraMemoryController = BuyExtraMemoryController;
__decorate([
    (0, common_1.Post)('/memory'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiUnauthorizedResponse)({
        description: 'Invalid token or refreshToken or the user is not authenticated.',
    }),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    (0, common_1.Redirect)(),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, currentUser_decorator_1.CurrentUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [User_dto_1.CreateBuyMemoryDto,
        User_schema_1.User]),
    __metadata("design:returntype", Promise)
], BuyExtraMemoryController.prototype, "buyExtraMemory", null);
__decorate([
    (0, common_1.Get)('/memory/success/:id'),
    __param(0, (0, common_1.Param)()),
    __param(0, (0, validID_decorator_1.ValidId)()),
    __param(1, (0, common_1.Res)({ passthrough: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], BuyExtraMemoryController.prototype, "paymentSuccess", null);
__decorate([
    (0, common_1.Get)('/memory/failed/:id'),
    __param(0, (0, common_1.Param)()),
    __param(0, (0, validID_decorator_1.ValidId)()),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], BuyExtraMemoryController.prototype, "paymentFailed", null);
exports.BuyExtraMemoryController = BuyExtraMemoryController = __decorate([
    (0, common_1.Controller)('users'),
    (0, swagger_1.ApiTags)('Users'),
    __metadata("design:paramtypes", [users_service_1.UsersService,
        products_service_1.ProductsService])
], BuyExtraMemoryController);
//# sourceMappingURL=buyExtraMemory.controller.js.map