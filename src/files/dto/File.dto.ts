import {
  IsNotEmpty,
  IsEmail,
  IsString,
  IsOptional,
  IsBoolean,
  IsArray,
  ValidateNested,
  IsIn,
} from 'class-validator';
import { Transform, Type } from 'class-transformer';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Folder } from 'src/schemas/Folder.schema';
import { File } from 'src/schemas/File.schema';

export class Permission {
  @ApiProperty({
    description: 'Role of the permission (viewer or editor)',
    example: 'editor',
  })
  @IsString()
  @IsIn(['viewer', 'editor'])
  role: string;

  @ApiProperty({
    description: 'Email associated with the permission',
    example: 'user@example.com',
  })
  @IsString()
  @IsEmail()
  email: string;
}

export class EditFileDto {
  @ApiProperty({
    description: 'File name',
    example: 'My file',
  })
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiPropertyOptional({
    description: 'Indicates whether the file is private',
    example: true,
  })
  @IsOptional()
  @IsBoolean()
  @Transform((value) => Boolean(value))
  isPrivate?: boolean;
}
export class CreateFileDto extends EditFileDto {
  @ApiProperty({
    description: 'Root folder containing this file',
    example: '66471a9ce46805482dd323a4',
  })
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  rootFolder: string;
}
export class FileDto extends CreateFileDto {
  @ApiProperty({
    description: 'Permissions granted to users for accessing the file',
    example: [{ role: 'viewer', email: 'user@example.com' }],
  })
  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => Permission)
  permissions: Permission[];
}

export class FilesResponseDto {
  @ApiProperty({ type: [Folder] })
  folders: Folder[];

  @ApiProperty({ type: [File] })
  files: File[];
}
