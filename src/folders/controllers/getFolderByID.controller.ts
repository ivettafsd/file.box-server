import {
  Controller,
  UseGuards,
  Param,
  Get,
  NotFoundException,
} from '@nestjs/common';
import { FolderByIDResponseDto } from '../dto/Folder.dto';
import { AuthGuard } from '../../guards/auth.guard';
import { FoldersService } from '../folders.service';
import { CurrentUser } from 'src/users/decorators/currentUser.decorator';
import { User } from 'src/schemas/User.schema';
import { Types } from 'mongoose';
import { FilesService } from 'src/files/files.service';
import {
  ApiBearerAuth,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { ValidId } from '../decorators/validID.decorator';

@Controller('folders')
export class GetFolderByIDController {
  constructor(
    private readonly foldersService: FoldersService,
    private readonly filesService: FilesService,
  ) {}

  @Get('/:id')
  @ApiTags('Folders')
  @ApiOperation({
    summary:
      'Get a folder by ID if the current user is the owner or has permissions',
  })
  @ApiBearerAuth()
  @ApiParam({
    name: 'id',
    description: 'Folder ID',
    example: '66471a9ce46805482dd323a4',
  })
  @ApiOkResponse({
    description:
      'Folder retrieved successfully, along with lists of permissive folders and files',
    type: FolderByIDResponseDto,
  })
  @ApiNotFoundResponse({
    description: 'Folder not found or does not belong to the current user',
  })
  @ApiUnauthorizedResponse({
    description:
      'Invalid token or refreshToken or the user is not authenticated.',
  })
  @UseGuards(AuthGuard)
  async getFolderById(@Param() @ValidId() { id }, @CurrentUser() user: User) {
    const filter = {
      _id: new Types.ObjectId(id),
      $or: [{ owner: user._id }, { 'permissions.email': user.email }],
    };
    const foundFolder = await this.foldersService.findOne(filter);

    if (!foundFolder) {
      throw new NotFoundException(
        'Folder not found or does not belong to the current user',
      );
    }
    if (id === user.mainFolder.toString()) {
      const permissiveFilter = {
        $and: [{ 'permissions.email': user.email }, { isPrivate: false }],
      };
      const permissiveFolders =
        await this.foldersService.find(permissiveFilter);

      const permissiveFiles = await this.filesService.find(permissiveFilter);

      return {
        ...foundFolder.toObject(),
        permissiveFolders,
        permissiveFiles,
      };
    } else {
      return foundFolder;
    }
  }
}
