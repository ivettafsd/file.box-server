import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';
import { ConfigService } from '@nestjs/config';
import { ConfigModule } from './config/config.module';
import { UserModule } from './users/users.module';
import { FilesModule } from './files/files.module';
import { FoldersModule } from './folders/folders.module';
import { GoogleStrategy } from './strategies/google.strategy';
import { EmailModule } from './email/email.module';
import { ProductsModule } from './products/products.module';
@Module({
  imports: [
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get<string>('MONGO_DB'),
      }),
      inject: [ConfigService],
    }),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    EmailModule,
    UserModule,
    FilesModule,
    FoldersModule,
    ProductsModule,
  ],
  controllers: [],
  providers: [GoogleStrategy],
})
export class AppModule {}
