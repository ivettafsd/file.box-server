"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthUserWithGoogleController = void 0;
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const swagger_1 = require("@nestjs/swagger");
let AuthUserWithGoogleController = class AuthUserWithGoogleController {
    async googleAuth() { }
};
exports.AuthUserWithGoogleController = AuthUserWithGoogleController;
__decorate([
    (0, common_1.Get)('/google'),
    (0, swagger_1.ApiOperation)({ summary: 'Authenticate with Google' }),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('google')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], AuthUserWithGoogleController.prototype, "googleAuth", null);
exports.AuthUserWithGoogleController = AuthUserWithGoogleController = __decorate([
    (0, common_1.Controller)('users'),
    (0, swagger_1.ApiTags)('Users')
], AuthUserWithGoogleController);
//# sourceMappingURL=authUserWithGoogle.controller.js.map