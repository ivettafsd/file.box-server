"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeleteFolderController = void 0;
const common_1 = require("@nestjs/common");
const auth_guard_1 = require("../../guards/auth.guard");
const folders_service_1 = require("./../folders.service");
const currentUser_decorator_1 = require("../../users/decorators/currentUser.decorator");
const User_schema_1 = require("../../schemas/User.schema");
const mongoose_1 = require("mongoose");
const swagger_1 = require("@nestjs/swagger");
const validID_decorator_1 = require("../decorators/validID.decorator");
let DeleteFolderController = class DeleteFolderController {
    constructor(foldersService) {
        this.foldersService = foldersService;
    }
    async removeFolder({ id }, user, res) {
        if (user.mainFolder.toString() === id) {
            throw new common_1.ForbiddenException('Operation forbidden: Cannot remove the root folder');
        }
        const filter = { _id: new mongoose_1.Types.ObjectId(id), owner: user._id };
        const deletedFolder = await this.foldersService.findOneAndDelete(filter);
        if (!deletedFolder) {
            throw new common_1.NotFoundException('Folder not found or does not belong to the current user');
        }
        res.status(200).json({ message: 'Removed folder success' });
    }
};
exports.DeleteFolderController = DeleteFolderController;
__decorate([
    (0, common_1.Delete)('/:id'),
    (0, swagger_1.ApiOperation)({
        summary: 'Delete a folder by its ID',
    }),
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiParam)({
        name: 'id',
        description: 'Folder ID',
        example: '66471a9ce46805482dd323a4',
    }),
    (0, swagger_1.ApiOkResponse)({
        description: 'Removed folder success',
    }),
    (0, swagger_1.ApiNotFoundResponse)({
        description: 'Folder not found or does not belong to the current user',
    }),
    (0, swagger_1.ApiBadRequestResponse)({
        description: 'Invalid folder ID',
    }),
    (0, swagger_1.ApiForbiddenResponse)({
        description: 'Operation forbidden: Cannot remove the root folder',
    }),
    (0, swagger_1.ApiUnauthorizedResponse)({
        description: 'Invalid token or refreshToken or the user is not authenticated.',
    }),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    __param(0, (0, common_1.Param)()),
    __param(0, (0, validID_decorator_1.ValidId)()),
    __param(1, (0, currentUser_decorator_1.CurrentUser)()),
    __param(2, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, User_schema_1.User, Object]),
    __metadata("design:returntype", Promise)
], DeleteFolderController.prototype, "removeFolder", null);
exports.DeleteFolderController = DeleteFolderController = __decorate([
    (0, common_1.Controller)('folders'),
    (0, swagger_1.ApiTags)('Folders'),
    __metadata("design:paramtypes", [folders_service_1.FoldersService])
], DeleteFolderController);
//# sourceMappingURL=deleteFolder.controller.js.map