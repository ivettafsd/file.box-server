export declare class AuthUserDto {
    email: string;
    password: string;
    name?: string;
    avatar?: string;
}
export declare class ChangeUserPasswordDto {
    oldPassword: string;
    newPassword: string;
}
export declare class SignUpUserDto extends AuthUserDto {
}
export declare class SignInUserDto extends AuthUserDto {
}
