"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfigModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const Joi = require("joi");
let ConfigModule = class ConfigModule {
};
exports.ConfigModule = ConfigModule;
exports.ConfigModule = ConfigModule = __decorate([
    (0, common_1.Module)({
        imports: [
            config_1.ConfigModule.forRoot({
                isGlobal: true,
                validationSchema: Joi.object({
                    MONGO_DB: Joi.string().required(),
                    AWS_S3_REGION: Joi.string().required(),
                    AWS_S3_BUCKET: Joi.string().required(),
                    AWS_ACCESS_KEY_ID: Joi.string().required(),
                    AWS_SECRET_ACCESS_KEY: Joi.string().required(),
                    GOOGLE_CLIENT_ID: Joi.string().required(),
                    GOOGLE_CLIENT_SECRET: Joi.string().required(),
                    BASE_URL: Joi.string().required(),
                    FRONT_BASE_URL: Joi.string().required(),
                    SENDGRID_API_KEY: Joi.string().required(),
                    PROJECT_EMAIL: Joi.string().required(),
                    PROJECT: Joi.string().required(),
                    STRIPE_PUBLISH_KEY: Joi.string().required(),
                    STRIPE_SECRET_KEY: Joi.string().required(),
                    STRIPE_CUSTOMER: Joi.string().required(),
                }),
            }),
        ],
        providers: [config_1.ConfigService],
        exports: [config_1.ConfigModule],
    })
], ConfigModule);
//# sourceMappingURL=config.module.js.map