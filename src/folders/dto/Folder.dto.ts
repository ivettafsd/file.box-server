import { ApiProperty, ApiPropertyOptional, OmitType } from '@nestjs/swagger';
import { Transform, Type } from 'class-transformer';
import {
  IsNotEmpty,
  IsString,
  IsOptional,
  IsBoolean,
  IsEmail,
  IsArray,
  ValidateNested,
  IsIn,
} from 'class-validator';
import { File } from 'src/schemas/File.schema';
import { Folder } from 'src/schemas/Folder.schema';

export class Permission {
  @ApiProperty({
    description: 'Role of the permission (viewer or editor)',
    example: 'editor',
  })
  @IsString()
  @IsIn(['viewer', 'editor'])
  role: string;

  @ApiProperty({
    description: 'Email associated with the permission',
    example: 'user@example.com',
  })
  @IsString()
  @IsEmail()
  email: string;
}

export class EditFolderDto {
  @ApiProperty({
    description: 'Folder title',
    example: 'My folder',
  })
  @IsNotEmpty()
  @IsString()
  title: string;

  @ApiPropertyOptional({
    description: 'Indicates whether the folder is private',
    example: true,
  })
  @IsOptional()
  @IsBoolean()
  @Transform((value) => Boolean(value))
  isPrivate?: boolean;
}

export class CreateFolderDto extends EditFolderDto {
  @ApiProperty({
    description: 'Root folder containing this folder',
    example: '66471a9ce46805482dd323a4',
  })
  @IsNotEmpty()
  @IsString()
  rootFolder: string;
}

export class FolderDto extends CreateFolderDto {
  @ApiProperty({
    description: 'Permissions granted to users for accessing the folder',
    example: [{ role: 'viewer', email: 'user@example.com' }],
  })
  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => Permission)
  permissions: Permission[];
}

export class FoldersResponseDto {
  @ApiProperty({ type: [Folder] })
  folders: Folder[];

  @ApiProperty({ type: [Folder] })
  permissiveFolders: Folder[];
}

export class FolderByIDResponseDto extends OmitType(Folder, [
  'files',
  'folders',
] as const) {
  @ApiProperty({ type: [Folder], description: 'Array of folder objects' })
  folders: Folder[];

  @ApiProperty({ type: [File], description: 'Array of file objects' })
  files: File[];

  @ApiPropertyOptional({
    type: [Folder],
    description: 'Array of permissive folder objects',
  })
  permissiveFolders: PermissiveFolder[];

  @ApiPropertyOptional({
    type: [File],
    description: 'Array of permissive file objects',
  })
  permissiveFiles: File[];
}

export interface PermissiveFolder extends Omit<Folder, 'files' | 'folders'> {}
