import { PaymentsService } from 'src/payments/payments.service';
import { UsersService } from 'src/users/users.service';
export declare class BuyProductSuccessController {
    private paymentsService;
    private usersService;
    constructor(paymentsService: PaymentsService, usersService: UsersService);
    paymentSuccess({ id }: {
        id: any;
    }): Promise<{
        url: string;
    }>;
}
