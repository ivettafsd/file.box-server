"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FoldersModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const Folder_schema_1 = require("../schemas/Folder.schema");
const User_schema_1 = require("../schemas/User.schema");
const folders_service_1 = require("./folders.service");
const jwt_1 = require("@nestjs/jwt");
const users_service_1 = require("../users/users.service");
const File_schema_1 = require("../schemas/File.schema");
const files_service_1 = require("../files/files.service");
const createFolder_controller_1 = require("./controllers/createFolder.controller");
const changeFolderPermissions_controller_1 = require("./controllers/changeFolderPermissions.controller");
const editFolder_controller_1 = require("./controllers/editFolder.controller");
const deleteFolder_controller_1 = require("./controllers/deleteFolder.controller");
const getFolders_controller_1 = require("./controllers/getFolders.controller");
const getFolderByID_controller_1 = require("./controllers/getFolderByID.controller");
let FoldersModule = class FoldersModule {
};
exports.FoldersModule = FoldersModule;
exports.FoldersModule = FoldersModule = __decorate([
    (0, common_1.Module)({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                {
                    name: Folder_schema_1.Folder.name,
                    schema: Folder_schema_1.FolderSchema,
                },
                {
                    name: User_schema_1.User.name,
                    schema: User_schema_1.UserSchema,
                },
                {
                    name: File_schema_1.File.name,
                    schema: File_schema_1.FileSchema,
                },
            ]),
        ],
        controllers: [
            createFolder_controller_1.CreateFolderController,
            changeFolderPermissions_controller_1.ChangeFolderPermissionsController,
            editFolder_controller_1.EditFolderController,
            deleteFolder_controller_1.DeleteFolderController,
            getFolders_controller_1.GetFoldersController,
            getFolderByID_controller_1.GetFolderByIDController,
        ],
        providers: [folders_service_1.FoldersService, jwt_1.JwtService, users_service_1.UsersService, files_service_1.FilesService],
    })
], FoldersModule);
//# sourceMappingURL=folders.module.js.map