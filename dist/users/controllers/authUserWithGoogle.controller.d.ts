export declare class AuthUserWithGoogleController {
    googleAuth(): Promise<void>;
}
