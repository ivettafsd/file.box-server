import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { File, FileSchema } from '../schemas/File.schema';
import { User, UserSchema } from '../schemas/User.schema';
import { GetFilesController } from './controllers/getFiles.controller';
import { FilesService } from './files.service';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { FoldersService } from 'src/folders/folders.service';
import { Folder, FolderSchema } from 'src/schemas/Folder.schema';
import { AddFileController } from './controllers/addFile.controller';
import { ChangeFilePermissionsController } from './controllers/changeFilePermissions.controller';
import { EditFileController } from './controllers/editFile.controller';
import { DeleteFileController } from './controllers/deleteFile.controller';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: File.name,
        schema: FileSchema,
      },
      {
        name: User.name,
        schema: UserSchema,
      },
      {
        name: Folder.name,
        schema: FolderSchema,
      },
    ]),
  ],
  controllers: [
    AddFileController,
    ChangeFilePermissionsController,
    GetFilesController,
    EditFileController,
    DeleteFileController,
  ],
  providers: [FilesService, JwtService, UsersService, FoldersService],
})
export class FilesModule {}
