import { ForbiddenException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { Folder } from '../schemas/Folder.schema';
import { File } from 'src/schemas/File.schema';
import { FilesService } from 'src/files/files.service';

@Injectable()
export class FoldersService {
  constructor(
    @InjectModel(Folder.name) private foldersModel: Model<Folder>,
    @InjectModel(File.name) private filesModel: Model<File>,
    private filesService: FilesService,
  ) {}
  async create(createDto) {
    const createdFolder = await this.foldersModel.create(createDto);
    return createdFolder;
  }
  async findOneAndUpdate(filter, updateDto) {
    const updatedFolder = await this.foldersModel.findOneAndUpdate(
      filter,
      updateDto,
      { new: true },
    );
    if (!updatedFolder) {
      throw new ForbiddenException(
        'Folder not found or does not belong to the current user',
      );
    }
    return updatedFolder;
  }

  async findOneAndDelete(filter) {
    const foundFolder = await this.findOne(filter);

    if (!foundFolder) {
      throw new ForbiddenException(
        'Folder not found or does not belong to the current user',
      );
    }

    await this.foldersModel.findOneAndDelete(filter);
    await this.deleteSubfolders(foundFolder.folders);
    await this.filesService.deleteSubfiles(foundFolder.files);
    await this.foldersModel.findOneAndUpdate(
      { _id: foundFolder.rootFolder, owner: foundFolder.owner },
      { $pull: { folders: foundFolder._id } },
    );

    return foundFolder;
  }
  async find(filters) {
    return await this.foldersModel
      .find(filters)
      .populate('folders', '', this.foldersModel)
      .populate('files', '', this.filesModel);
  }
  async deleteSubfolders(folderIds: Types.ObjectId[]): Promise<void> {
    for (const folder of folderIds) {
      await this.findOneAndDelete({ _id: folder._id });
    }
  }
  async updateSubfolders(
    folderIds: Types.ObjectId[],
    updateDto,
  ): Promise<void> {
    for (const folder of folderIds) {
      await this.findOneAndUpdate({ _id: folder._id }, updateDto);
    }
  }
  async findOne(filters) {
    if (!filters) {
      return null;
    }
    return await this.foldersModel
      .findOne(filters)
      .populate('folders', '', this.foldersModel)
      .populate('files', '', this.filesModel);
  }
}
