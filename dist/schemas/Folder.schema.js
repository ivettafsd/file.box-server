"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FolderSchema = exports.Folder = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const swagger_1 = require("@nestjs/swagger");
const mongoose_2 = require("mongoose");
class Permission {
}
__decorate([
    (0, mongoose_1.Prop)({ required: true, enum: ['viewer', 'editor'] }),
    __metadata("design:type", String)
], Permission.prototype, "role", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], Permission.prototype, "email", void 0);
let Folder = class Folder extends mongoose_2.Document {
    constructor() {
        super(...arguments);
        this.permissions = [];
    }
};
exports.Folder = Folder;
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'Folder ID',
        example: '66471a9ce46805482dd323a4',
    }),
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", mongoose_2.Types.ObjectId)
], Folder.prototype, "_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'Owner of the folder (User ID)',
        example: '66471a9ce46805482dd323a4',
    }),
    (0, mongoose_1.Prop)({ type: mongoose_2.Types.ObjectId, ref: 'User', required: true }),
    __metadata("design:type", mongoose_2.Types.ObjectId)
], Folder.prototype, "owner", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'Folder title',
        example: 'My folder',
    }),
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], Folder.prototype, "title", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'Files contained in the folder',
        example: ['66471a9ce46805482dd323a4', '66471a9ce46805482dd323a5'],
    }),
    (0, mongoose_1.Prop)({ type: [{ type: mongoose_2.Types.ObjectId, ref: 'File' }], default: [] }),
    __metadata("design:type", Array)
], Folder.prototype, "files", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'Folders contained in the folder',
        example: ['66471a9ce46805482dd323a4', '66471a9ce46805482dd323a5'],
    }),
    (0, mongoose_1.Prop)({ type: [{ type: mongoose_2.Types.ObjectId, ref: 'this' }], default: [] }),
    __metadata("design:type", Array)
], Folder.prototype, "folders", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'Root folder containing this folder',
        example: '66471a9ce46805482dd323a4',
    }),
    (0, mongoose_1.Prop)({ type: mongoose_2.Types.ObjectId, ref: 'this', required: false }),
    __metadata("design:type", mongoose_2.Types.ObjectId)
], Folder.prototype, "rootFolder", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'Indicates whether the folder is private',
        example: true,
    }),
    (0, mongoose_1.Prop)({ required: true, default: true }),
    __metadata("design:type", Boolean)
], Folder.prototype, "isPrivate", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'Permissions granted to users for accessing the folder',
        example: [{ role: 'viewer', email: 'user@example.com' }],
    }),
    (0, mongoose_1.Prop)([Permission]),
    __metadata("design:type", Array)
], Folder.prototype, "permissions", void 0);
exports.Folder = Folder = __decorate([
    (0, mongoose_1.Schema)({ versionKey: false, timestamps: true })
], Folder);
exports.FolderSchema = mongoose_1.SchemaFactory.createForClass(Folder);
//# sourceMappingURL=Folder.schema.js.map