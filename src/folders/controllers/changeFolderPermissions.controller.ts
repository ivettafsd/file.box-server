import {
  Controller,
  Body,
  UseGuards,
  Param,
  Patch,
  NotFoundException,
  ForbiddenException,
} from '@nestjs/common';
import { Permission } from './../dto/Folder.dto';
import { AuthGuard } from '../../guards/auth.guard';
import { FoldersService } from './../folders.service';
import { CurrentUser } from 'src/users/decorators/currentUser.decorator';
import { User } from 'src/schemas/User.schema';
import { Types } from 'mongoose';
import { FilesService } from 'src/files/files.service';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiBody,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { ValidId } from '../decorators/validID.decorator';
import { Folder } from 'src/schemas/Folder.schema';

@Controller('folders')
@ApiTags('Folders')
export class ChangeFolderPermissionsController {
  constructor(
    private readonly foldersService: FoldersService,
    private readonly filesService: FilesService,
  ) {}

  @Patch('/:id/permissions')
  @ApiOperation({
    summary: 'Update permissions for a folder',
  })
  @ApiBearerAuth()
  @ApiParam({
    name: 'id',
    description: 'Folder ID',
    example: '66471a9ce46805482dd323a4',
  })
  @ApiBody({
    description: 'Array containing the updated permissions for the folder',
    type: [Permission],
  })
  @ApiOkResponse({
    description: 'Permissions updated successfully for the folder',
    type: Folder,
  })
  @ApiNotFoundResponse({
    description: 'Folder not found or does not belong to the current user',
  })
  @ApiBadRequestResponse({
    description: 'Invalid folder ID',
  })
  @ApiForbiddenResponse({
    description:
      'User does not have sufficient rights or permissions for this operation' +
      ' OR ' +
      'Operation forbidden: Cannot share permissions for the root folder',
  })
  @ApiUnauthorizedResponse({
    description:
      'Invalid token or refreshToken or the user is not authenticated.',
  })
  @UseGuards(AuthGuard)
  async updateFolderPermissions(
    @Param() @ValidId() { id },
    @Body() folderDto: [Permission],
    @CurrentUser() user: User,
  ) {
    const folderId = new Types.ObjectId(id);
    const filter = { _id: folderId, owner: user._id };

    if (user.mainFolder.toString() === id) {
      throw new ForbiddenException(
        'Operation forbidden: Cannot share permissions for the root folder',
      );
    }

    const updatedFolder = await this.foldersService.findOneAndUpdate(filter, {
      $set: { permissions: folderDto },
    });
    if (!updatedFolder) {
      throw new NotFoundException(
        'Folder not found or does not belong to the current user',
      );
    }
    await this.foldersService.updateSubfolders(updatedFolder.folders, {
      ...folderDto,
      isPrivate: false,
    });
    await this.filesService.updateSubfiles(updatedFolder.files, {
      ...folderDto,
      isPrivate: false,
    });
    const foundFolder = await this.foldersService.findOne(filter);
    return foundFolder;
  }
}
