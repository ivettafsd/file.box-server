"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BuyProductFailedController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const validID_decorator_1 = require("../../users/decorators/validID.decorator");
const payments_service_1 = require("../../payments/payments.service");
const { FRONT_BASE_URL } = process.env;
let BuyProductFailedController = class BuyProductFailedController {
    constructor(paymentsService) {
        this.paymentsService = paymentsService;
    }
    async paymentFailed({ id }) {
        await this.paymentsService.failedSession(id);
        return { url: `${FRONT_BASE_URL}/files?error=payment` };
    }
};
exports.BuyProductFailedController = BuyProductFailedController;
__decorate([
    (0, common_1.Get)('/buy/failed/:id'),
    __param(0, (0, common_1.Param)()),
    __param(0, (0, validID_decorator_1.ValidId)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], BuyProductFailedController.prototype, "paymentFailed", null);
exports.BuyProductFailedController = BuyProductFailedController = __decorate([
    (0, common_1.Controller)('products'),
    (0, swagger_1.ApiTags)('Products'),
    (0, swagger_1.ApiExcludeController)(),
    __metadata("design:paramtypes", [payments_service_1.PaymentsService])
], BuyProductFailedController);
//# sourceMappingURL=buyProductFailed.controller.js.map