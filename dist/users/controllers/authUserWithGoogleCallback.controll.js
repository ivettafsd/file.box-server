"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthUsersWithGoogleCallbackController = void 0;
const common_1 = require("@nestjs/common");
const auth_service_1 = require("./../auth.service");
const passport_1 = require("@nestjs/passport");
const swagger_1 = require("@nestjs/swagger");
const { FRONT_BASE_URL } = process.env;
let AuthUsersWithGoogleCallbackController = class AuthUsersWithGoogleCallbackController {
    constructor(authService) {
        this.authService = authService;
    }
    async googleAuthRedirect(req) {
        const authorizedUser = await this.authService.googleLogin(req);
        if (authorizedUser) {
            return { url: `${FRONT_BASE_URL}/google/${authorizedUser.token}` };
        }
        else {
            return { url: `${FRONT_BASE_URL}/google/error` };
        }
    }
};
exports.AuthUsersWithGoogleCallbackController = AuthUsersWithGoogleCallbackController;
__decorate([
    (0, common_1.Get)('/google/callback'),
    (0, swagger_1.ApiExcludeEndpoint)(),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('google')),
    (0, common_1.Redirect)(),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AuthUsersWithGoogleCallbackController.prototype, "googleAuthRedirect", null);
exports.AuthUsersWithGoogleCallbackController = AuthUsersWithGoogleCallbackController = __decorate([
    (0, common_1.Controller)('users'),
    __metadata("design:paramtypes", [auth_service_1.AuthService])
], AuthUsersWithGoogleCallbackController);
//# sourceMappingURL=authUserWithGoogleCallback.controll.js.map