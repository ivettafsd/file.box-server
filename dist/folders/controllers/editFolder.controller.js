"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EditFolderController = void 0;
const common_1 = require("@nestjs/common");
const Folder_dto_1 = require("./../dto/Folder.dto");
const auth_guard_1 = require("../../guards/auth.guard");
const folders_service_1 = require("./../folders.service");
const currentUser_decorator_1 = require("../../users/decorators/currentUser.decorator");
const User_schema_1 = require("../../schemas/User.schema");
const mongoose_1 = require("mongoose");
const swagger_1 = require("@nestjs/swagger");
const Folder_schema_1 = require("../../schemas/Folder.schema");
const validID_decorator_1 = require("../decorators/validID.decorator");
let EditFolderController = class EditFolderController {
    constructor(foldersService) {
        this.foldersService = foldersService;
    }
    async updateFolder({ id }, folderDto, user) {
        const folderId = new mongoose_1.Types.ObjectId(id);
        const filter = {
            _id: folderId,
            $or: [
                { owner: user._id },
                { 'permissions.email': user.email, 'permissions.role': 'editor' },
            ],
        };
        if (user.mainFolder.toString() === id) {
            throw new common_1.ForbiddenException('Operation forbidden: Cannot update properties of the root folder');
        }
        const foundFolder = await this.foldersService.findOne(filter);
        if (!foundFolder) {
            throw new common_1.NotFoundException('Folder not found or does not belong to the current user');
        }
        if (foundFolder.owner.toString() !== user._id.toString() &&
            folderDto.hasOwnProperty('isPrivate')) {
            throw new common_1.ForbiddenException('Folder does not belong to the current user for changing its private status');
        }
        const foundFolderWithSuchTitle = await this.foldersService.findOne({
            title: folderDto.title,
            _id: { $ne: folderId },
            rootFolder: new mongoose_1.Types.ObjectId(foundFolder.rootFolder),
        });
        if (foundFolderWithSuchTitle) {
            throw new common_1.ConflictException('Folder with such a title already exists in this folder');
        }
        const updatedFolder = await this.foldersService.findOneAndUpdate(filter, {
            ...folderDto,
            ...(folderDto.hasOwnProperty('isPrivate') &&
                folderDto.isPrivate && { permissions: [] }),
        });
        return updatedFolder;
    }
};
exports.EditFolderController = EditFolderController;
__decorate([
    (0, common_1.Patch)('/:id'),
    (0, swagger_1.ApiOperation)({
        summary: 'Update properties of a folder',
    }),
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiParam)({
        name: 'id',
        description: 'Folder ID',
        example: '66471a9ce46805482dd323a4',
    }),
    (0, swagger_1.ApiOkResponse)({
        description: 'Properties of a folder updated successfully',
        type: Folder_schema_1.Folder,
    }),
    (0, swagger_1.ApiNotFoundResponse)({
        description: 'Folder not found or does not belong to the current user',
    }),
    (0, swagger_1.ApiBadRequestResponse)({
        description: 'Invalid folder ID',
    }),
    (0, swagger_1.ApiConflictResponse)({
        description: 'Folder with such a title already exists in this folder',
    }),
    (0, swagger_1.ApiForbiddenResponse)({
        description: 'Operation forbidden: Cannot update properties of the root folder',
    }),
    (0, swagger_1.ApiUnauthorizedResponse)({
        description: 'Invalid token or refreshToken or the user is not authenticated.',
    }),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    __param(0, (0, common_1.Param)()),
    __param(0, (0, validID_decorator_1.ValidId)()),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, currentUser_decorator_1.CurrentUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Folder_dto_1.EditFolderDto,
        User_schema_1.User]),
    __metadata("design:returntype", Promise)
], EditFolderController.prototype, "updateFolder", null);
exports.EditFolderController = EditFolderController = __decorate([
    (0, common_1.Controller)('folders'),
    (0, swagger_1.ApiTags)('Folders'),
    __metadata("design:paramtypes", [folders_service_1.FoldersService])
], EditFolderController);
//# sourceMappingURL=editFolder.controller.js.map