import {
  Controller,
  Body,
  UseGuards,
  Param,
  Patch,
  NotFoundException,
  ForbiddenException,
  ConflictException,
} from '@nestjs/common';
import { EditFolderDto } from './../dto/Folder.dto';
import { AuthGuard } from '../../guards/auth.guard';
import { FoldersService } from './../folders.service';
import { CurrentUser } from 'src/users/decorators/currentUser.decorator';
import { User } from 'src/schemas/User.schema';
import { Types } from 'mongoose';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiConflictResponse,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Folder } from 'src/schemas/Folder.schema';
import { ValidId } from '../decorators/validID.decorator';

@Controller('folders')
@ApiTags('Folders')
export class EditFolderController {
  constructor(private readonly foldersService: FoldersService) {}

  @Patch('/:id')
  @ApiOperation({
    summary: 'Update properties of a folder',
  })
  @ApiBearerAuth()
  @ApiParam({
    name: 'id',
    description: 'Folder ID',
    example: '66471a9ce46805482dd323a4',
  })
  @ApiOkResponse({
    description: 'Properties of a folder updated successfully',
    type: Folder,
  })
  @ApiNotFoundResponse({
    description: 'Folder not found or does not belong to the current user',
  })
  @ApiBadRequestResponse({
    description: 'Invalid folder ID',
  })
  @ApiConflictResponse({
    description: 'Folder with such a title already exists in this folder',
  })
  @ApiForbiddenResponse({
    description:
      'Operation forbidden: Cannot update properties of the root folder',
  })
  @ApiUnauthorizedResponse({
    description:
      'Invalid token or refreshToken or the user is not authenticated.',
  })
  @UseGuards(AuthGuard)
  async updateFolder(
    @Param() @ValidId() { id },
    @Body() folderDto: EditFolderDto,
    @CurrentUser() user: User,
  ) {
    const folderId = new Types.ObjectId(id);
    const filter = {
      _id: folderId,
      $or: [
        { owner: user._id },
        { 'permissions.email': user.email, 'permissions.role': 'editor' },
      ],
    };
    if (user.mainFolder.toString() === id) {
      throw new ForbiddenException(
        'Operation forbidden: Cannot update properties of the root folder',
      );
    }

    const foundFolder = await this.foldersService.findOne(filter);
    if (!foundFolder) {
      throw new NotFoundException(
        'Folder not found or does not belong to the current user',
      );
    }
    if (
      foundFolder.owner.toString() !== user._id.toString() &&
      folderDto.hasOwnProperty('isPrivate')
    ) {
      throw new ForbiddenException(
        'Folder does not belong to the current user for changing its private status',
      );
    }

    const foundFolderWithSuchTitle = await this.foldersService.findOne({
      title: folderDto.title,
      _id: { $ne: folderId },
      rootFolder: new Types.ObjectId(foundFolder.rootFolder),
    });

    if (foundFolderWithSuchTitle) {
      throw new ConflictException(
        'Folder with such a title already exists in this folder',
      );
    }
    const updatedFolder = await this.foldersService.findOneAndUpdate(filter, {
      ...folderDto,
      ...(folderDto.hasOwnProperty('isPrivate') &&
        folderDto.isPrivate && { permissions: [] }),
    });
    return updatedFolder;
  }
}
