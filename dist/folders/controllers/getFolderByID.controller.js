"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetFolderByIDController = void 0;
const common_1 = require("@nestjs/common");
const Folder_dto_1 = require("../dto/Folder.dto");
const auth_guard_1 = require("../../guards/auth.guard");
const folders_service_1 = require("../folders.service");
const currentUser_decorator_1 = require("../../users/decorators/currentUser.decorator");
const User_schema_1 = require("../../schemas/User.schema");
const mongoose_1 = require("mongoose");
const files_service_1 = require("../../files/files.service");
const swagger_1 = require("@nestjs/swagger");
const validID_decorator_1 = require("../decorators/validID.decorator");
let GetFolderByIDController = class GetFolderByIDController {
    constructor(foldersService, filesService) {
        this.foldersService = foldersService;
        this.filesService = filesService;
    }
    async getFolderById({ id }, user) {
        const filter = {
            _id: new mongoose_1.Types.ObjectId(id),
            $or: [{ owner: user._id }, { 'permissions.email': user.email }],
        };
        const foundFolder = await this.foldersService.findOne(filter);
        if (!foundFolder) {
            throw new common_1.NotFoundException('Folder not found or does not belong to the current user');
        }
        if (id === user.mainFolder.toString()) {
            const permissiveFilter = {
                $and: [{ 'permissions.email': user.email }, { isPrivate: false }],
            };
            const permissiveFolders = await this.foldersService.find(permissiveFilter);
            const permissiveFiles = await this.filesService.find(permissiveFilter);
            return {
                ...foundFolder.toObject(),
                permissiveFolders,
                permissiveFiles,
            };
        }
        else {
            return foundFolder;
        }
    }
};
exports.GetFolderByIDController = GetFolderByIDController;
__decorate([
    (0, common_1.Get)('/:id'),
    (0, swagger_1.ApiTags)('Folders'),
    (0, swagger_1.ApiOperation)({
        summary: 'Get a folder by ID if the current user is the owner or has permissions',
    }),
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiParam)({
        name: 'id',
        description: 'Folder ID',
        example: '66471a9ce46805482dd323a4',
    }),
    (0, swagger_1.ApiOkResponse)({
        description: 'Folder retrieved successfully, along with lists of permissive folders and files',
        type: Folder_dto_1.FolderByIDResponseDto,
    }),
    (0, swagger_1.ApiNotFoundResponse)({
        description: 'Folder not found or does not belong to the current user',
    }),
    (0, swagger_1.ApiUnauthorizedResponse)({
        description: 'Invalid token or refreshToken or the user is not authenticated.',
    }),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    __param(0, (0, common_1.Param)()),
    __param(0, (0, validID_decorator_1.ValidId)()),
    __param(1, (0, currentUser_decorator_1.CurrentUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, User_schema_1.User]),
    __metadata("design:returntype", Promise)
], GetFolderByIDController.prototype, "getFolderById", null);
exports.GetFolderByIDController = GetFolderByIDController = __decorate([
    (0, common_1.Controller)('folders'),
    __metadata("design:paramtypes", [folders_service_1.FoldersService,
        files_service_1.FilesService])
], GetFolderByIDController);
//# sourceMappingURL=getFolderByID.controller.js.map