import { Controller, Get, UseGuards, Req, Redirect } from '@nestjs/common';
import { AuthService } from './../auth.service';
import { AuthGuard as AuthGoogleGuard } from '@nestjs/passport';
import { ApiExcludeEndpoint } from '@nestjs/swagger';
const { FRONT_BASE_URL } = process.env;

@Controller('users')
export class AuthUsersWithGoogleCallbackController {
  constructor(private authService: AuthService) {}

  @Get('/google/callback')
  @ApiExcludeEndpoint()
  @UseGuards(AuthGoogleGuard('google'))
  @Redirect()
  async googleAuthRedirect(@Req() req) {
    const authorizedUser = await this.authService.googleLogin(req);
    if (authorizedUser) {
      return { url: `${FRONT_BASE_URL}/google/${authorizedUser.token}` };
    } else {
      return { url: `${FRONT_BASE_URL}/google/error` };
    }
  }
}
