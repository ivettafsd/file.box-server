import { Controller, UseGuards, Get } from '@nestjs/common';
import { FoldersResponseDto } from './../dto/Folder.dto';
import { AuthGuard } from '../../guards/auth.guard';
import { FoldersService } from './../folders.service';
import { CurrentUser } from 'src/users/decorators/currentUser.decorator';
import { User } from 'src/schemas/User.schema';

import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';

@Controller('folders')
@ApiTags('Folders')
export class GetFoldersController {
  constructor(private readonly foldersService: FoldersService) {}

  @Get()
  @ApiOperation({
    summary:
      'Get all folders owned by the user and folders where the current user has permissions',
  })
  @ApiBearerAuth()
  @ApiOkResponse({
    description:
      'List of folders owned by the user and folders where the current user has permissions',
    type: FoldersResponseDto,
  })
  @ApiUnauthorizedResponse({
    description:
      'Invalid token or refreshToken or the user is not authenticated.',
  })
  @UseGuards(AuthGuard)
  async getFolders(@CurrentUser() user: User) {
    const filter = { owner: user._id, rootFolder: user.mainFolder };
    const folders = await this.foldersService.find(filter);
    const permissiveFilter = {
      $and: [{ 'permissions.email': user.email }, { isPrivate: false }],
    };
    const permissiveFolders = await this.foldersService.find(permissiveFilter);

    return {
      folders,
      permissiveFolders,
    };
  }
}
