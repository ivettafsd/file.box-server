"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FilesModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const File_schema_1 = require("../schemas/File.schema");
const User_schema_1 = require("../schemas/User.schema");
const getFiles_controller_1 = require("./controllers/getFiles.controller");
const files_service_1 = require("./files.service");
const jwt_1 = require("@nestjs/jwt");
const users_service_1 = require("../users/users.service");
const folders_service_1 = require("../folders/folders.service");
const Folder_schema_1 = require("../schemas/Folder.schema");
const addFile_controller_1 = require("./controllers/addFile.controller");
const changeFilePermissions_controller_1 = require("./controllers/changeFilePermissions.controller");
const editFile_controller_1 = require("./controllers/editFile.controller");
const deleteFile_controller_1 = require("./controllers/deleteFile.controller");
let FilesModule = class FilesModule {
};
exports.FilesModule = FilesModule;
exports.FilesModule = FilesModule = __decorate([
    (0, common_1.Module)({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                {
                    name: File_schema_1.File.name,
                    schema: File_schema_1.FileSchema,
                },
                {
                    name: User_schema_1.User.name,
                    schema: User_schema_1.UserSchema,
                },
                {
                    name: Folder_schema_1.Folder.name,
                    schema: Folder_schema_1.FolderSchema,
                },
            ]),
        ],
        controllers: [
            addFile_controller_1.AddFileController,
            changeFilePermissions_controller_1.ChangeFilePermissionsController,
            getFiles_controller_1.GetFilesController,
            editFile_controller_1.EditFileController,
            deleteFile_controller_1.DeleteFileController,
        ],
        providers: [files_service_1.FilesService, jwt_1.JwtService, users_service_1.UsersService, folders_service_1.FoldersService],
    })
], FilesModule);
//# sourceMappingURL=files.module.js.map