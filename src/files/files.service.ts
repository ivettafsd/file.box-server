import {
  Injectable,
  ForbiddenException,
  NotFoundException,
} from '@nestjs/common';
import {
  S3Client,
  PutObjectCommand,
  DeleteObjectCommand,
} from '@aws-sdk/client-s3';
import { ConfigService } from '@nestjs/config';
import { Model, Types } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { File } from '../schemas/File.schema';
import { UsersService } from 'src/users/users.service';
@Injectable()
export class FilesService {
  private readonly s3Client = new S3Client({
    region: this.configService.getOrThrow('AWS_S3_REGION'),
  });
  constructor(
    private readonly configService: ConfigService,
    @InjectModel(File.name) private filesModel: Model<File>,
    private readonly usersService: UsersService,
  ) {}
  async uploadRemoteFile(fileName: string, file: Buffer) {
    try {
      await this.s3Client.send(
        new PutObjectCommand({
          Bucket: this.configService.getOrThrow('AWS_S3_BUCKET'),
          Key: fileName,
          Body: file,
        }),
      );
      const url = `https://${this.configService.getOrThrow('AWS_S3_BUCKET')}.s3.amazonaws.com/${fileName}`;
      return url;
    } catch (error) {
      throw new Error(`Error uploading file to S3: ${error.message}`);
    }
  }
  async deleteRemoteFile(fileName: string) {
    try {
      const deletedFile = await this.s3Client.send(
        new DeleteObjectCommand({
          Bucket: this.configService.getOrThrow('AWS_S3_BUCKET'),
          Key: fileName,
        }),
      );
      return deletedFile;
    } catch (error) {
      throw new Error(
        `Error deleting file ${fileName} from S3: ${error.message}`,
      );
    }
  }
  async create(createDto) {
    const createdFile = await this.filesModel.create(createDto);
    const updatedUser = await this.usersService.update(createdFile.owner, {
      $inc: { availableMemory: -createdFile.size },
    });
    if (!updatedUser) {
      throw new NotFoundException('User not found');
    }
    return createdFile;
  }
  async findOneAndUpdate(filter, updateDto) {
    const updatedFile = await this.filesModel.findOneAndUpdate(
      filter,
      updateDto,
      { new: true },
    );
    if (!updatedFile) {
      throw new ForbiddenException(
        'File not found or does not belong to the current user',
      );
    }
    return updatedFile;
  }
  async findOneAndDelete(filter) {
    const foundFile = await this.findOne(filter);

    if (!foundFile) {
      throw new ForbiddenException(
        'File not found or does not belong to the current user',
      );
    }
    const fileName = filter._id.toString();
    await this.deleteRemoteFile(fileName);
    await this.filesModel.findOneAndDelete(filter);

    const updatedUser = await this.usersService.update(foundFile.owner, {
      $inc: { availableMemory: +foundFile.size },
    });
    if (!updatedUser) {
      throw new NotFoundException('User not found');
    }
    return foundFile;
  }
  async deleteSubfiles(fileIds: Types.ObjectId[]): Promise<void> {
    for (const file of fileIds) {
      await this.findOneAndDelete({ _id: file._id });
    }
  }
  async updateSubfiles(fileIds: Types.ObjectId[], updateDto): Promise<void> {
    for (const file of fileIds) {
      await this.findOneAndUpdate({ _id: file._id }, updateDto);
    }
  }
  async find(filters: {}) {
    return await this.filesModel.find(filters);
  }
  async findOne(filters) {
    if (!filters) {
      return null;
    }
    return await this.filesModel.findOne(filters);
  }
}
