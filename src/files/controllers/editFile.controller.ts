import {
  Controller,
  UseGuards,
  Body,
  Param,
  Patch,
  NotFoundException,
  ForbiddenException,
  ConflictException,
} from '@nestjs/common';
import { Types } from 'mongoose';
import { CurrentUser } from 'src/users/decorators/currentUser.decorator';
import { AuthGuard } from '../../guards/auth.guard';
import { User } from 'src/schemas/User.schema';
import { FilesService } from './../files.service';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiConflictResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { EditFileDto } from '../dto/File.dto';
import { ValidId } from '../decorators/validID.decorator';
import { File } from 'src/schemas/File.schema';

@Controller('files')
@ApiTags('Files')
export class EditFileController {
  constructor(private fileService: FilesService) {}

  @Patch('/:id')
  @ApiOperation({
    summary: 'Update properties of a file',
  })
  @ApiBearerAuth()
  @ApiParam({
    name: 'id',
    description: 'File ID',
    example: '66471a9ce46805482dd323a4',
  })
  @ApiOkResponse({
    description: 'Properties of a file updated successfully',
    type: File,
  })
  @ApiNotFoundResponse({
    description: 'File not found or does not belong to the current user',
  })
  @ApiBadRequestResponse({
    description: 'Invalid file ID',
  })
  @ApiConflictResponse({
    description: 'File with such a name already exists in this folder',
  })
  @ApiUnauthorizedResponse({
    description:
      'Invalid token or refreshToken or the user is not authenticated.',
  })
  @UseGuards(AuthGuard)
  async updateFile(
    @Param() @ValidId() { id },
    @Body() fileDto: EditFileDto,
    @CurrentUser() user: User,
  ) {
    const fileId = new Types.ObjectId(id);
    const filter = {
      _id: fileId,
      $or: [
        { owner: user._id },
        { 'permissions.email': user.email, 'permissions.role': 'editor' },
      ],
    };
    const foundFile = await this.fileService.findOne(filter);
    if (!foundFile) {
      throw new NotFoundException(
        'File not found or does not belong to the current user',
      );
    }
    if (
      foundFile.owner.toString() !== user._id.toString() &&
      fileDto.hasOwnProperty('isPrivate')
    ) {
      throw new ForbiddenException(
        'File does not belong to the current user for changing its private status',
      );
    }
    const foundFileWithSuchTitle = await this.fileService.findOne({
      name: fileDto.name,
      _id: { $ne: fileId },
      rootFolder: new Types.ObjectId(foundFile.rootFolder),
    });

    if (foundFileWithSuchTitle) {
      throw new ConflictException(
        'File with such a name already exists in this folder',
      );
    }
    const updatedFile = await this.fileService.findOneAndUpdate(
      filter,
      fileDto,
    );
    return updatedFile;
  }
}
