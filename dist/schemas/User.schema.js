"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserSchema = exports.User = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const swagger_1 = require("@nestjs/swagger");
const mongoose_2 = require("mongoose");
let User = class User {
};
exports.User = User;
__decorate([
    (0, swagger_1.ApiProperty)({ description: 'User ID', example: '66471a9ce46805482dd323a4' }),
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", mongoose_2.Types.ObjectId)
], User.prototype, "_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'User email',
        example: 'test@gmail.com',
    }),
    (0, mongoose_1.Prop)({ unique: true, required: true }),
    __metadata("design:type", String)
], User.prototype, "email", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], User.prototype, "password", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ description: 'User name', example: 'Ivetta' }),
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], User.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'User avatar',
        example: 'https://ivetta-dashkova-file-box.s3.eu-north-1.amazonaws.com/unknown.png',
    }),
    (0, mongoose_1.Prop)({
        required: true,
        default: 'https://ivetta-dashkova-file-box.s3.eu-north-1.amazonaws.com/unknown.png',
    }),
    __metadata("design:type", String)
], User.prototype, "avatar", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'User access token',
        example: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY2NDcxYTljZTQ2ODA1NDgyZGQzMjNhNCIsImlhdCI6MTcxNTkzNTkwMSwiZXhwIjoxNzE1OTM2MjAxfQ.LKYRlc6MYzauS2vpZt31wfFRGGmu2VqZVJEi-L4fqE4',
    }),
    (0, mongoose_1.Prop)({ required: false }),
    __metadata("design:type", String)
], User.prototype, "token", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'User refresh token',
        example: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY2NDcxYTljZTQ2ODA1NDgyZGQzMjNhNCIsImlhdCI6MTcxNTkzNTkwMSwiZXhwIjoxNzE2NTQwNzAxfQ.AjlkO1MRuYNINnk9B3P_flY7RFyrzoURFKMxn8nDEGI',
    }),
    (0, mongoose_1.Prop)({ required: false }),
    __metadata("design:type", String)
], User.prototype, "refreshToken", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'User main folder ID',
        example: '66471a9ce46805482dd323a4',
    }),
    (0, mongoose_1.Prop)({ required: false }),
    __metadata("design:type", mongoose_2.Types.ObjectId)
], User.prototype, "mainFolder", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'Available memory for the user to save files',
        example: 10485760,
    }),
    (0, mongoose_1.Prop)({ required: true, default: 10485760 }),
    __metadata("design:type", Number)
], User.prototype, "availableMemory", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'User creation timestamp',
        example: '2024-01-01T00:00:00.000Z',
    }),
    __metadata("design:type", Date)
], User.prototype, "createdAt", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'User update timestamp',
        example: '2024-01-01T00:00:00.000Z',
    }),
    __metadata("design:type", Date)
], User.prototype, "updatedAt", void 0);
exports.User = User = __decorate([
    (0, mongoose_1.Schema)({ versionKey: false, timestamps: true })
], User);
exports.UserSchema = mongoose_1.SchemaFactory.createForClass(User);
//# sourceMappingURL=User.schema.js.map