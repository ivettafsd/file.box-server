import {
  Controller,
  Post,
  Body,
  UseGuards,
  ForbiddenException,
  ConflictException,
  NotFoundException,
} from '@nestjs/common';
import { CreateFolderDto } from './../dto/Folder.dto';
import { AuthGuard } from '../../guards/auth.guard';
import { FoldersService } from './../folders.service';
import { CurrentUser } from 'src/users/decorators/currentUser.decorator';
import { User } from 'src/schemas/User.schema';
import { Types } from 'mongoose';
import {
  ApiBearerAuth,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Folder } from 'src/schemas/Folder.schema';

@Controller('folders')
@ApiTags('Folders')
export class CreateFolderController {
  constructor(private readonly foldersService: FoldersService) {}

  @Post()
  @ApiOperation({ summary: 'Create a new folder' })
  @ApiBearerAuth()
  @ApiCreatedResponse({
    description: 'Folder successfully created',
    type: Folder,
  })
  @ApiNotFoundResponse({
    description: 'Folder not found',
  })
  @ApiConflictResponse({
    description: 'Folder with such a title already exists in this folder',
  })
  @ApiForbiddenResponse({
    description:
      'User does not have sufficient rights or permissions for this operation',
  })
  @ApiUnauthorizedResponse({
    description:
      'Invalid token or refreshToken or the user is not authenticated.',
  })
  @UseGuards(AuthGuard)
  async createFolder(
    @Body() folderDto: CreateFolderDto,
    @CurrentUser() user: User,
  ) {
    const rootFolder = new Types.ObjectId(folderDto.rootFolder);

    const foundRootFolder = await this.foldersService.findOne({
      _id: rootFolder,
    });
    if (!foundRootFolder) {
      throw new NotFoundException('Folder not found');
    }

    if (
      foundRootFolder.owner.toString() !== user._id.toString() &&
      !foundRootFolder.permissions.find(
        (permit) => permit.email === user.email && permit.role === 'editor',
      )
    ) {
      throw new ForbiddenException(
        'User does not have sufficient rights or permissions for this operation',
      );
    }

    const foundFolder = await this.foldersService.findOne({
      title: folderDto.title,
      rootFolder: new Types.ObjectId(folderDto.rootFolder),
    });

    if (foundFolder) {
      throw new ConflictException('Such a folder already exists');
    }

    const createdFolder = await this.foldersService.create({
      title: folderDto.title,
      owner: foundRootFolder.owner,
      _id: new Types.ObjectId(),
      isPrivate:
        foundRootFolder.owner.toString() !== user._id.toString()
          ? foundRootFolder.isPrivate
          : folderDto.isPrivate,
      rootFolder: new Types.ObjectId(folderDto.rootFolder),
      permissions: foundRootFolder.permissions,
    });

    if (createdFolder) {
      await this.foldersService.findOneAndUpdate(
        {
          _id: new Types.ObjectId(folderDto.rootFolder),
        },
        {
          $push: { folders: createdFolder._id },
        },
      );
    }
    return createdFolder;
  }
}
