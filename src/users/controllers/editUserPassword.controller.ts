import { Body, Controller, Patch, UseGuards } from '@nestjs/common';
import { User } from '../../schemas/User.schema';
import { AuthGuard } from '../../guards/auth.guard';
import { CurrentUser } from './../decorators/currentUser.decorator';
import { AuthService } from './../auth.service';
import {
  ApiNotFoundResponse,
  ApiTags,
  ApiOkResponse,
  ApiUnauthorizedResponse,
  ApiBearerAuth,
  ApiOperation,
  ApiBadRequestResponse,
} from '@nestjs/swagger';
import { ChangeUserPasswordDto } from '../dto/User.dto';

@Controller('users')
@ApiTags('Users')
export class EditUserPasswordController {
  constructor(private authService: AuthService) {}

  @Patch('/edit/password')
  @ApiOperation({
    summary: 'Change user password',
  })
  @ApiBearerAuth()
  @ApiOkResponse({
    description:
      'The password has been successfully changed. Returns the updated user object.',
    type: User,
  })
  @ApiUnauthorizedResponse({
    description:
      'Unauthorized. The provided token is invalid or expired, or the user is not authenticated.',
  })
  @ApiNotFoundResponse({
    description: 'User not found. The user does not exist in the database.',
  })
  @ApiBadRequestResponse({
    description: 'Email or password invalid',
  })
  @UseGuards(AuthGuard)
  async changePassword(
    @Body() changePasswordDto: ChangeUserPasswordDto,
    @CurrentUser() user: User,
  ) {
    const refreshedUser = await this.authService.changeUserPassword(
      changePasswordDto,
      user,
    );
    return refreshedUser;
  }
}
