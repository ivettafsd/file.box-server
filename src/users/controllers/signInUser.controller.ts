import { Controller, Post, Body } from '@nestjs/common';
import { SignInUserDto } from './../dto/User.dto';
import { User } from '../../schemas/User.schema';
import { AuthService } from './../auth.service';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiTags,
  ApiOperation,
} from '@nestjs/swagger';

@Controller('users')
@ApiTags('Users')
export class SignInUserController {
  constructor(private authService: AuthService) {}

  @Post('/signin')
  @ApiOperation({ summary: 'Sign in an existing user' })
  @ApiCreatedResponse({
    description: 'User successfully signed in',
    type: User,
  })
  @ApiNotFoundResponse({
    description: 'User not found',
  })
  @ApiBadRequestResponse({
    description: 'Email or password invalid',
  })
  async signInUser(@Body() signInUserDto: SignInUserDto) {
    const authorizedUser = await this.authService.signIn(signInUserDto);
    return authorizedUser;
  }
}
