/// <reference types="mongoose/types/aggregate" />
/// <reference types="mongoose/types/callback" />
/// <reference types="mongoose/types/collection" />
/// <reference types="mongoose/types/connection" />
/// <reference types="mongoose/types/cursor" />
/// <reference types="mongoose/types/document" />
/// <reference types="mongoose/types/error" />
/// <reference types="mongoose/types/expressions" />
/// <reference types="mongoose/types/helpers" />
/// <reference types="mongoose/types/middlewares" />
/// <reference types="mongoose/types/indexes" />
/// <reference types="mongoose/types/models" />
/// <reference types="mongoose/types/mongooseoptions" />
/// <reference types="mongoose/types/pipelinestage" />
/// <reference types="mongoose/types/populate" />
/// <reference types="mongoose/types/query" />
/// <reference types="mongoose/types/schemaoptions" />
/// <reference types="mongoose/types/schematypes" />
/// <reference types="mongoose/types/session" />
/// <reference types="mongoose/types/types" />
/// <reference types="mongoose/types/utility" />
/// <reference types="mongoose/types/validation" />
/// <reference types="mongoose/types/virtuals" />
/// <reference types="mongoose/types/inferschematype" />
import { Model, Types } from 'mongoose';
import { User } from '../schemas/User.schema';
export declare class UsersService {
    private usersModel;
    constructor(usersModel: Model<User>);
    create(createDto: any): Promise<{
        _id: Types.ObjectId;
        email: string;
        name: string;
        avatar: string;
        token: string;
        refreshToken: string;
        mainFolder: Types.ObjectId;
        availableMemory: number;
        createdAt: Date;
        updatedAt: Date;
    }>;
    update(_id: Types.ObjectId, attrs: any): Promise<{
        _id: Types.ObjectId;
        email: string;
        name: string;
        avatar: string;
        token: string;
        refreshToken: string;
        mainFolder: Types.ObjectId;
        availableMemory: number;
        createdAt: Date;
        updatedAt: Date;
    }>;
    findById(_id: Types.ObjectId): Promise<{
        _id: Types.ObjectId;
        email: string;
        name: string;
        avatar: string;
        token: string;
        refreshToken: string;
        mainFolder: Types.ObjectId;
        availableMemory: number;
        createdAt: Date;
        updatedAt: Date;
    }>;
    findOne(filters: Partial<User>): Promise<import("mongoose").Document<unknown, {}, User> & User & Required<{
        _id: Types.ObjectId;
    }>>;
}
