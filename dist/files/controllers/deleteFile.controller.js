"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeleteFileController = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const currentUser_decorator_1 = require("../../users/decorators/currentUser.decorator");
const auth_guard_1 = require("../../guards/auth.guard");
const User_schema_1 = require("../../schemas/User.schema");
const files_service_1 = require("./../files.service");
const folders_service_1 = require("../../folders/folders.service");
const swagger_1 = require("@nestjs/swagger");
const validID_decorator_1 = require("../decorators/validID.decorator");
let DeleteFileController = class DeleteFileController {
    constructor(fileService, foldersService) {
        this.fileService = fileService;
        this.foldersService = foldersService;
    }
    async removeFile({ id }, user, res) {
        const filter = { _id: new mongoose_1.Types.ObjectId(id), owner: user._id };
        const deletedFile = await this.fileService.findOneAndDelete(filter);
        if (!deletedFile) {
            throw new common_1.NotFoundException('File not found or does not belong to the current user');
        }
        const rootFolder = new mongoose_1.Types.ObjectId(deletedFile.rootFolder);
        await this.foldersService.findOneAndUpdate({ _id: rootFolder, owner: user._id }, {
            $pull: { files: deletedFile._id },
        });
        res.status(200).json({ message: 'Removed file success' });
    }
};
exports.DeleteFileController = DeleteFileController;
__decorate([
    (0, common_1.Delete)('/:id'),
    (0, swagger_1.ApiOperation)({
        summary: 'Delete a file by its ID',
    }),
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiParam)({
        name: 'id',
        description: 'File ID',
        example: '66471a9ce46805482dd323a4',
    }),
    (0, swagger_1.ApiOkResponse)({
        description: 'Removed file success',
    }),
    (0, swagger_1.ApiNotFoundResponse)({
        description: 'File not found or does not belong to the current user',
    }),
    (0, swagger_1.ApiBadRequestResponse)({
        description: 'Invalid file ID',
    }),
    (0, swagger_1.ApiUnauthorizedResponse)({
        description: 'Invalid token or refreshToken or the user is not authenticated.',
    }),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    __param(0, (0, common_1.Param)()),
    __param(0, (0, validID_decorator_1.ValidId)()),
    __param(1, (0, currentUser_decorator_1.CurrentUser)()),
    __param(2, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, User_schema_1.User, Object]),
    __metadata("design:returntype", Promise)
], DeleteFileController.prototype, "removeFile", null);
exports.DeleteFileController = DeleteFileController = __decorate([
    (0, common_1.Controller)('files'),
    (0, swagger_1.ApiTags)('Files'),
    __metadata("design:paramtypes", [files_service_1.FilesService,
        folders_service_1.FoldersService])
], DeleteFileController);
//# sourceMappingURL=deleteFile.controller.js.map