"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthGuard = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const jwt_1 = require("@nestjs/jwt");
const users_service_1 = require("../users/users.service");
let AuthGuard = class AuthGuard {
    constructor(jwtService, usersService) {
        this.jwtService = jwtService;
        this.usersService = usersService;
    }
    async canActivate(context) {
        const request = context.switchToHttp().getRequest();
        const { authorization = '' } = request.headers;
        const [bearer, token] = authorization.split(' ');
        if (bearer !== 'Bearer' || !token) {
            this.handleAuthError('Invalid token');
        }
        try {
            const id = await this.verifyToken(token, process.env.SECRET_KEY);
            const userId = new mongoose_1.Types.ObjectId(id);
            const foundUser = await this.usersService.findById(userId);
            if (!foundUser || foundUser.token !== token) {
                this.handleAuthError('Invalid token');
            }
            request.user = foundUser;
            return true;
        }
        catch (error) {
            if (error.name === 'UnauthorizedException') {
                await this.refreshToken(request);
                return true;
            }
            else {
                this.handleAuthError('Invalid token');
            }
        }
    }
    async refreshToken(request) {
        const { authorization = '' } = request.headers;
        const [, refreshToken] = authorization.split(' ');
        if (!refreshToken) {
            this.handleAuthError('No refresh token provided');
        }
        try {
            const id = await this.verifyToken(refreshToken, process.env.REFRESH_SECRET_KEY);
            const userId = new mongoose_1.Types.ObjectId(id);
            const foundUser = await this.usersService.findById(userId);
            if (!foundUser || foundUser.refreshToken !== refreshToken) {
                this.handleAuthError('Invalid refresh token');
            }
            request.user = foundUser;
            return true;
        }
        catch (error) {
            this.handleAuthError();
        }
    }
    async verifyToken(token, secret) {
        try {
            const { id } = await this.jwtService.verifyAsync(token, { secret });
            return id;
        }
        catch (error) {
            this.handleAuthError();
        }
    }
    handleAuthError(message = 'Authentication failed') {
        throw new common_1.UnauthorizedException(message);
    }
};
exports.AuthGuard = AuthGuard;
exports.AuthGuard = AuthGuard = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [jwt_1.JwtService,
        users_service_1.UsersService])
], AuthGuard);
//# sourceMappingURL=auth.guard.js.map