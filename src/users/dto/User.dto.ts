import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class AuthUserDto {
  @ApiProperty({
    description: 'User email as a string with a valid email address format',
    example: 'test@gmail.com',
  })
  @IsNotEmpty()
  @IsString()
  @IsEmail()
  email: string;

  @ApiProperty({
    description: 'User password as a string',
    example: '1234567',
  })
  @IsNotEmpty()
  @IsString()
  password: string;

  @IsOptional()
  @IsString()
  name?: string;

  @IsOptional()
  @IsString()
  avatar?: string;
}
export class ChangeUserPasswordDto {
  @ApiProperty({
    description: 'User old password as a string',
    example: '1234567',
  })
  @IsNotEmpty()
  @IsString()
  oldPassword: string;

  @ApiProperty({
    description: 'User new password as a string',
    example: 'Qwerty12',
  })
  @IsNotEmpty()
  @IsString()
  newPassword: string;
}
export class SignUpUserDto extends AuthUserDto {}

export class SignInUserDto extends AuthUserDto {}
