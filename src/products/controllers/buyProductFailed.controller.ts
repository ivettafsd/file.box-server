import { Controller, Get, Param } from '@nestjs/common';
import { ApiExcludeController, ApiTags } from '@nestjs/swagger';
import { ValidId } from '../../users/decorators/validID.decorator';
import { PaymentsService } from 'src/payments/payments.service';
const { FRONT_BASE_URL } = process.env;

@Controller('products')
@ApiTags('Products')
@ApiExcludeController()
export class BuyProductFailedController {
  constructor(private paymentsService: PaymentsService) {}

  @Get('/buy/failed/:id')
  async paymentFailed(@Param() @ValidId() { id }) {
    await this.paymentsService.failedSession(id);

    return { url: `${FRONT_BASE_URL}/files?error=payment` };
  }
}
