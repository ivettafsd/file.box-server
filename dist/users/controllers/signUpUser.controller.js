"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SignUpUserController = void 0;
const common_1 = require("@nestjs/common");
const User_dto_1 = require("./../dto/User.dto");
const User_schema_1 = require("../../schemas/User.schema");
const auth_service_1 = require("./../auth.service");
const swagger_1 = require("@nestjs/swagger");
let SignUpUserController = class SignUpUserController {
    constructor(authService) {
        this.authService = authService;
    }
    async signUpUser(signUpUserDto) {
        await this.authService.signUp(signUpUserDto);
        const authorizedUser = await this.authService.signIn(signUpUserDto);
        return authorizedUser;
    }
};
exports.SignUpUserController = SignUpUserController;
__decorate([
    (0, common_1.Post)('/signup'),
    (0, swagger_1.ApiOperation)({ summary: 'Sign up a new user' }),
    (0, swagger_1.ApiCreatedResponse)({
        description: 'User successfully signed up',
        type: User_schema_1.User,
    }),
    (0, swagger_1.ApiConflictResponse)({
        description: 'Such email already exists',
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [User_dto_1.SignUpUserDto]),
    __metadata("design:returntype", Promise)
], SignUpUserController.prototype, "signUpUser", null);
exports.SignUpUserController = SignUpUserController = __decorate([
    (0, common_1.Controller)('users'),
    (0, swagger_1.ApiTags)('Users'),
    __metadata("design:paramtypes", [auth_service_1.AuthService])
], SignUpUserController);
//# sourceMappingURL=signUpUser.controller.js.map