import { Controller, Get, UseGuards } from '@nestjs/common';
import { User } from '../../schemas/User.schema';
import { AuthGuard } from '../../guards/auth.guard';
import { CurrentUser } from './../decorators/currentUser.decorator';
import {
  ApiNotFoundResponse,
  ApiTags,
  ApiOkResponse,
  ApiUnauthorizedResponse,
  ApiBearerAuth,
  ApiOperation,
} from '@nestjs/swagger';

@Controller('users')
@ApiTags('Users')
export class WhoAmIController {
  @Get('/whoami')
  @ApiOperation({
    summary: 'Get information about the currently authenticated user',
  })
  @ApiBearerAuth()
  @ApiOkResponse({
    description:
      'Returns the currently authenticated user based on the provided authentication token.',
    type: User,
  })
  @ApiUnauthorizedResponse({
    description: 'Invalid token or the user is not authenticated.',
  })
  @ApiNotFoundResponse({
    description: 'User not found',
  })
  @UseGuards(AuthGuard)
  whoAmI(@CurrentUser() user: User) {
    return user;
  }
}
