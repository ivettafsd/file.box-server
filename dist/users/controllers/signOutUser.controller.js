"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SignOutUserController = void 0;
const common_1 = require("@nestjs/common");
const User_schema_1 = require("../../schemas/User.schema");
const auth_guard_1 = require("../../guards/auth.guard");
const currentUser_decorator_1 = require("../decorators/currentUser.decorator");
const auth_service_1 = require("../auth.service");
const swagger_1 = require("@nestjs/swagger");
let SignOutUserController = class SignOutUserController {
    constructor(authService) {
        this.authService = authService;
    }
    async signOut(user, res) {
        await this.authService.signOut(user._id);
        res.status(200).json({ message: 'Sign out success' });
    }
};
exports.SignOutUserController = SignOutUserController;
__decorate([
    (0, common_1.Post)('/signout'),
    (0, swagger_1.ApiOperation)({
        summary: 'Sign out the currently authenticated user',
    }),
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiOkResponse)({
        description: 'Sign out success',
    }),
    (0, swagger_1.ApiNotFoundResponse)({
        description: 'User not found',
    }),
    (0, swagger_1.ApiUnauthorizedResponse)({
        description: 'Invalid token or refreshToken or the user is not authenticated.',
    }),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    __param(0, (0, currentUser_decorator_1.CurrentUser)()),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [User_schema_1.User, Object]),
    __metadata("design:returntype", Promise)
], SignOutUserController.prototype, "signOut", null);
exports.SignOutUserController = SignOutUserController = __decorate([
    (0, common_1.Controller)('users'),
    (0, swagger_1.ApiTags)('Users'),
    __metadata("design:paramtypes", [auth_service_1.AuthService])
], SignOutUserController);
//# sourceMappingURL=signOutUser.controller.js.map