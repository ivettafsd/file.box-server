"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChangeFolderPermissionsController = void 0;
const common_1 = require("@nestjs/common");
const Folder_dto_1 = require("./../dto/Folder.dto");
const auth_guard_1 = require("../../guards/auth.guard");
const folders_service_1 = require("./../folders.service");
const currentUser_decorator_1 = require("../../users/decorators/currentUser.decorator");
const User_schema_1 = require("../../schemas/User.schema");
const mongoose_1 = require("mongoose");
const files_service_1 = require("../../files/files.service");
const swagger_1 = require("@nestjs/swagger");
const validID_decorator_1 = require("../decorators/validID.decorator");
const Folder_schema_1 = require("../../schemas/Folder.schema");
let ChangeFolderPermissionsController = class ChangeFolderPermissionsController {
    constructor(foldersService, filesService) {
        this.foldersService = foldersService;
        this.filesService = filesService;
    }
    async updateFolderPermissions({ id }, folderDto, user) {
        const folderId = new mongoose_1.Types.ObjectId(id);
        const filter = { _id: folderId, owner: user._id };
        if (user.mainFolder.toString() === id) {
            throw new common_1.ForbiddenException('Operation forbidden: Cannot share permissions for the root folder');
        }
        const updatedFolder = await this.foldersService.findOneAndUpdate(filter, {
            $set: { permissions: folderDto },
        });
        if (!updatedFolder) {
            throw new common_1.NotFoundException('Folder not found or does not belong to the current user');
        }
        await this.foldersService.updateSubfolders(updatedFolder.folders, {
            ...folderDto,
            isPrivate: false,
        });
        await this.filesService.updateSubfiles(updatedFolder.files, {
            ...folderDto,
            isPrivate: false,
        });
        const foundFolder = await this.foldersService.findOne(filter);
        return foundFolder;
    }
};
exports.ChangeFolderPermissionsController = ChangeFolderPermissionsController;
__decorate([
    (0, common_1.Patch)('/:id/permissions'),
    (0, swagger_1.ApiOperation)({
        summary: 'Update permissions for a folder',
    }),
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiParam)({
        name: 'id',
        description: 'Folder ID',
        example: '66471a9ce46805482dd323a4',
    }),
    (0, swagger_1.ApiBody)({
        description: 'Array containing the updated permissions for the folder',
        type: [Folder_dto_1.Permission],
    }),
    (0, swagger_1.ApiOkResponse)({
        description: 'Permissions updated successfully for the folder',
        type: Folder_schema_1.Folder,
    }),
    (0, swagger_1.ApiNotFoundResponse)({
        description: 'Folder not found or does not belong to the current user',
    }),
    (0, swagger_1.ApiBadRequestResponse)({
        description: 'Invalid folder ID',
    }),
    (0, swagger_1.ApiForbiddenResponse)({
        description: 'User does not have sufficient rights or permissions for this operation' +
            ' OR ' +
            'Operation forbidden: Cannot share permissions for the root folder',
    }),
    (0, swagger_1.ApiUnauthorizedResponse)({
        description: 'Invalid token or refreshToken or the user is not authenticated.',
    }),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    __param(0, (0, common_1.Param)()),
    __param(0, (0, validID_decorator_1.ValidId)()),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, currentUser_decorator_1.CurrentUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Array, User_schema_1.User]),
    __metadata("design:returntype", Promise)
], ChangeFolderPermissionsController.prototype, "updateFolderPermissions", null);
exports.ChangeFolderPermissionsController = ChangeFolderPermissionsController = __decorate([
    (0, common_1.Controller)('folders'),
    (0, swagger_1.ApiTags)('Folders'),
    __metadata("design:paramtypes", [folders_service_1.FoldersService,
        files_service_1.FilesService])
], ChangeFolderPermissionsController);
//# sourceMappingURL=changeFolderPermissions.controller.js.map