import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Folder, FolderSchema } from '../schemas/Folder.schema';
import { User, UserSchema } from '../schemas/User.schema';
import { FoldersService } from './folders.service';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { File, FileSchema } from 'src/schemas/File.schema';
import { FilesService } from 'src/files/files.service';
import { CreateFolderController } from './controllers/createFolder.controller';
import { ChangeFolderPermissionsController } from './controllers/changeFolderPermissions.controller';
import { EditFolderController } from './controllers/editFolder.controller';
import { DeleteFolderController } from './controllers/deleteFolder.controller';
import { GetFoldersController } from './controllers/getFolders.controller';
import { GetFolderByIDController } from './controllers/getFolderByID.controller';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: Folder.name,
        schema: FolderSchema,
      },
      {
        name: User.name,
        schema: UserSchema,
      },
      {
        name: File.name,
        schema: FileSchema,
      },
    ]),
  ],
  controllers: [
    CreateFolderController,
    ChangeFolderPermissionsController,
    EditFolderController,
    DeleteFolderController,
    GetFoldersController,
    GetFolderByIDController,
  ],
  providers: [FoldersService, JwtService, UsersService, FilesService],
})
export class FoldersModule {}
