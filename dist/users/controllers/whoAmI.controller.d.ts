import { User } from '../../schemas/User.schema';
export declare class WhoAmIController {
    whoAmI(user: User): User;
}
