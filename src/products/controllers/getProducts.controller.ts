import { Controller, UseGuards, Get } from '@nestjs/common';
import { AuthGuard } from '../../guards/auth.guard';
import { ProductsService } from '../products.service';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Product } from 'src/schemas/Product.schema';

@Controller('products')
@ApiTags('Products')
export class GetProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @Get()
  @ApiOperation({
    summary: 'Get all products',
  })
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'List of products',
    type: [Product],
  })
  @ApiUnauthorizedResponse({
    description:
      'Invalid token or refreshToken or the user is not authenticated.',
  })
  @UseGuards(AuthGuard)
  async getProducts() {
    const products = await this.productsService.find();

    return products;
  }
}
