import { PaymentsService } from '../payments.service';
export declare class CreatePaymentController {
    private readonly paymentsService;
    constructor(paymentsService: PaymentsService);
    getProducts(): Promise<any>;
}
