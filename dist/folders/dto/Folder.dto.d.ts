import { File } from 'src/schemas/File.schema';
import { Folder } from 'src/schemas/Folder.schema';
export declare class Permission {
    role: string;
    email: string;
}
export declare class EditFolderDto {
    title: string;
    isPrivate?: boolean;
}
export declare class CreateFolderDto extends EditFolderDto {
    rootFolder: string;
}
export declare class FolderDto extends CreateFolderDto {
    permissions: Permission[];
}
export declare class FoldersResponseDto {
    folders: Folder[];
    permissiveFolders: Folder[];
}
declare const FolderByIDResponseDto_base: import("@nestjs/common").Type<Omit<Folder, "files" | "folders">>;
export declare class FolderByIDResponseDto extends FolderByIDResponseDto_base {
    folders: Folder[];
    files: File[];
    permissiveFolders: PermissiveFolder[];
    permissiveFiles: File[];
}
export interface PermissiveFolder extends Omit<Folder, 'files' | 'folders'> {
}
export {};
