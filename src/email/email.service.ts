import { Injectable } from '@nestjs/common';
import { MailDataRequired } from '@sendgrid/mail';
import { SendGridClient } from './sendgrid-client';
import { EmailDto } from './dto/Email.dto';
const { PROJECT_EMAIL } = process.env;

const templates = {
  google: 'd-a7f931ba9c1e4f8190dcdb5f3e49779b'
};
@Injectable()
export class EmailService {
  constructor(private readonly sendGridClient: SendGridClient) {}

  async sendEmailWithTemplate(
    recipient: string,
    dynamic_template_data: EmailDto,
    subject: string,
    templateId: string,
  ): Promise<void> {
    const mail: MailDataRequired = {
      to: recipient,
      cc: 'example@mail.com',
      from: PROJECT_EMAIL,
      templateId: templates[templateId],
        dynamicTemplateData: { dynamic_template_data, subject },
    };
      await this.sendGridClient.send(mail);
  }
}
