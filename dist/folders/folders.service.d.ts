/// <reference types="mongoose/types/aggregate" />
/// <reference types="mongoose/types/callback" />
/// <reference types="mongoose/types/collection" />
/// <reference types="mongoose/types/connection" />
/// <reference types="mongoose/types/cursor" />
/// <reference types="mongoose/types/document" />
/// <reference types="mongoose/types/error" />
/// <reference types="mongoose/types/expressions" />
/// <reference types="mongoose/types/helpers" />
/// <reference types="mongoose/types/middlewares" />
/// <reference types="mongoose/types/indexes" />
/// <reference types="mongoose/types/models" />
/// <reference types="mongoose/types/mongooseoptions" />
/// <reference types="mongoose/types/pipelinestage" />
/// <reference types="mongoose/types/populate" />
/// <reference types="mongoose/types/query" />
/// <reference types="mongoose/types/schemaoptions" />
/// <reference types="mongoose/types/schematypes" />
/// <reference types="mongoose/types/session" />
/// <reference types="mongoose/types/types" />
/// <reference types="mongoose/types/utility" />
/// <reference types="mongoose/types/validation" />
/// <reference types="mongoose/types/virtuals" />
/// <reference types="mongoose/types/inferschematype" />
import { Model, Types } from 'mongoose';
import { Folder } from '../schemas/Folder.schema';
import { File } from 'src/schemas/File.schema';
import { FilesService } from 'src/files/files.service';
export declare class FoldersService {
    private foldersModel;
    private filesModel;
    private filesService;
    constructor(foldersModel: Model<Folder>, filesModel: Model<File>, filesService: FilesService);
    create(createDto: any): Promise<import("mongoose").Document<unknown, {}, Folder> & Folder & Required<{
        _id: Types.ObjectId;
    }>>;
    findOneAndUpdate(filter: any, updateDto: any): Promise<import("mongoose").Document<unknown, {}, Folder> & Folder & Required<{
        _id: Types.ObjectId;
    }>>;
    findOneAndDelete(filter: any): Promise<import("mongoose").Document<unknown, {}, Folder> & Folder & Required<{
        _id: Types.ObjectId;
    }>>;
    find(filters: any): Promise<(import("mongoose").Document<unknown, {}, Folder> & Folder & Required<{
        _id: Types.ObjectId;
    }>)[]>;
    deleteSubfolders(folderIds: Types.ObjectId[]): Promise<void>;
    updateSubfolders(folderIds: Types.ObjectId[], updateDto: any): Promise<void>;
    findOne(filters: any): Promise<import("mongoose").Document<unknown, {}, Folder> & Folder & Required<{
        _id: Types.ObjectId;
    }>>;
}
