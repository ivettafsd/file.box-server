/// <reference types="mongoose/types/aggregate" />
/// <reference types="mongoose/types/callback" />
/// <reference types="mongoose/types/collection" />
/// <reference types="mongoose/types/connection" />
/// <reference types="mongoose/types/cursor" />
/// <reference types="mongoose/types/document" />
/// <reference types="mongoose/types/error" />
/// <reference types="mongoose/types/expressions" />
/// <reference types="mongoose/types/helpers" />
/// <reference types="mongoose/types/middlewares" />
/// <reference types="mongoose/types/indexes" />
/// <reference types="mongoose/types/models" />
/// <reference types="mongoose/types/mongooseoptions" />
/// <reference types="mongoose/types/pipelinestage" />
/// <reference types="mongoose/types/populate" />
/// <reference types="mongoose/types/query" />
/// <reference types="mongoose/types/schemaoptions" />
/// <reference types="mongoose/types/schematypes" />
/// <reference types="mongoose/types/session" />
/// <reference types="mongoose/types/types" />
/// <reference types="mongoose/types/utility" />
/// <reference types="mongoose/types/validation" />
/// <reference types="mongoose/types/virtuals" />
/// <reference types="mongoose/types/inferschematype" />
import { JwtService } from '@nestjs/jwt';
import { EmailService } from '../email/email.service';
import { SignUpUserDto, SignInUserDto, ChangeUserPasswordDto } from './dto/User.dto';
import { Types } from 'mongoose';
import { UsersService } from './users.service';
import { FoldersService } from 'src/folders/folders.service';
export declare class AuthService {
    private usersService;
    private foldersService;
    private jwtService;
    private readonly emailService;
    constructor(usersService: UsersService, foldersService: FoldersService, jwtService: JwtService, emailService: EmailService);
    signUp(createDto: SignUpUserDto): Promise<{
        _id: Types.ObjectId;
        email: string;
        name: string;
        avatar: string;
        token: string;
        refreshToken: string;
        mainFolder: Types.ObjectId;
        availableMemory: number;
        createdAt: Date;
        updatedAt: Date;
    }>;
    signIn(createDto: SignInUserDto): Promise<{
        _id: Types.ObjectId;
        email: string;
        name: string;
        avatar: string;
        token: string;
        refreshToken: string;
        mainFolder: Types.ObjectId;
        availableMemory: number;
        createdAt: Date;
        updatedAt: Date;
    }>;
    changeUserPassword(changePasswordDto: ChangeUserPasswordDto, user: any): Promise<{
        _id: Types.ObjectId;
        email: string;
        name: string;
        avatar: string;
        token: string;
        refreshToken: string;
        mainFolder: Types.ObjectId;
        availableMemory: number;
        createdAt: Date;
        updatedAt: Date;
    }>;
    refreshTokens(userId: any): Promise<{
        _id: Types.ObjectId;
        email: string;
        name: string;
        avatar: string;
        token: string;
        refreshToken: string;
        mainFolder: Types.ObjectId;
        availableMemory: number;
        createdAt: Date;
        updatedAt: Date;
    }>;
    signOut(userId: any): Promise<{
        _id: Types.ObjectId;
        email: string;
        name: string;
        avatar: string;
        token: string;
        refreshToken: string;
        mainFolder: Types.ObjectId;
        availableMemory: number;
        createdAt: Date;
        updatedAt: Date;
    }>;
    googleLogin(req: any): Promise<{
        _id: Types.ObjectId;
        email: string;
        name: string;
        avatar: string;
        token: string;
        refreshToken: string;
        mainFolder: Types.ObjectId;
        availableMemory: number;
        createdAt: Date;
        updatedAt: Date;
    }>;
}
