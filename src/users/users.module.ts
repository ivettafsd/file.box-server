import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from '../schemas/User.schema';
import { UsersService } from './users.service';
import { AuthService } from './auth.service';
import { JwtService } from '@nestjs/jwt';
import { FoldersService } from 'src/folders/folders.service';
import { Folder, FolderSchema } from 'src/schemas/Folder.schema';
import { FileSchema, File } from 'src/schemas/File.schema';
import { FilesService } from 'src/files/files.service';
import { EmailModule } from 'src/email/email.module';
import { SignUpUserController } from './controllers/signUpUser.controller';
import { SignInUserController } from './controllers/signInUser.controller';
import { AuthUserWithGoogleController } from './controllers/authUserWithGoogle.controller';
import { AuthUsersWithGoogleCallbackController } from './controllers/authUserWithGoogleCallback.controll';
import { WhoAmIController } from './controllers/whoAmI.controller';
import { RefreshUserController } from './controllers/refreshUser.controller';
import { SignOutUserController } from './controllers/signOutUser.controller';
import { EditUserPasswordController } from './controllers/editUserPassword.controller';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: User.name,
        schema: UserSchema,
      },
      {
        name: Folder.name,
        schema: FolderSchema,
      },
      {
        name: File.name,
        schema: FileSchema,
      },
    ]),
    EmailModule,
  ],
  controllers: [
    SignUpUserController,
    SignInUserController,
    AuthUserWithGoogleController,
    AuthUsersWithGoogleCallbackController,
    WhoAmIController,
    RefreshUserController,
    SignOutUserController,
    EditUserPasswordController,
  ],
  providers: [
    UsersService,
    AuthService,
    JwtService,
    FoldersService,
    FilesService,
  ],
})
export class UserModule {}
