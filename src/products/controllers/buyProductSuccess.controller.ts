import { Controller, Get, Param, Redirect } from '@nestjs/common';
import { ApiExcludeController } from '@nestjs/swagger';
import { ValidId } from '../../users/decorators/validID.decorator';
import { PaymentsService } from 'src/payments/payments.service';
import { UsersService } from 'src/users/users.service';
const { FRONT_BASE_URL } = process.env;

@Controller('products')
@ApiExcludeController()
export class BuyProductSuccessController {
  constructor(
    private paymentsService: PaymentsService,
    private usersService: UsersService,
  ) {}

  @Get('/buy/success/:id')
  @Redirect()
  async paymentSuccess(@Param() @ValidId() { id }) {
    const updatedPayment = await this.paymentsService.successSession(id);

    await this.usersService.update(updatedPayment.payer, {
      $inc: { availableMemory: +updatedPayment.product.value },
    });

    return { url: FRONT_BASE_URL };
  }
}
