import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class EmailDto {
    @IsNotEmpty()
    @IsString()
    @IsEmail()
    email: string;

    @IsNotEmpty()
    @IsString()
    password: string;

    @IsString()
    name: string;

    @IsString()
    project: string;

    @IsNotEmpty()
    @IsString()
    @IsEmail()
    support: string;
}

