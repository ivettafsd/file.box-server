"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddFileController = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const currentUser_decorator_1 = require("../../users/decorators/currentUser.decorator");
const platform_express_1 = require("@nestjs/platform-express");
const auth_guard_1 = require("../../guards/auth.guard");
const User_schema_1 = require("../../schemas/User.schema");
const files_service_1 = require("./../files.service");
const File_dto_1 = require("./../dto/File.dto");
const folders_service_1 = require("../../folders/folders.service");
const swagger_1 = require("@nestjs/swagger");
const File_schema_1 = require("../../schemas/File.schema");
let AddFileController = class AddFileController {
    constructor(fileService, foldersService) {
        this.fileService = fileService;
        this.foldersService = foldersService;
    }
    async addFile(file, fileDto, user) {
        if (user.availableMemory - file.size < 0) {
            throw new common_1.ForbiddenException('Insufficient available memory to upload the file');
        }
        const rootFolder = new mongoose_1.Types.ObjectId(fileDto.rootFolder);
        const foundRootFolder = await this.foldersService.findOne({
            _id: rootFolder,
        });
        if (!foundRootFolder) {
            throw new common_1.NotFoundException('Folder not found');
        }
        if (foundRootFolder.owner.toString() !== user._id.toString() &&
            !foundRootFolder.permissions.find((permit) => permit.email === user.email && permit.role === 'editor')) {
            throw new common_1.ForbiddenException('User does not have sufficient rights or permissions for this operation');
        }
        const foundFile = await this.fileService.findOne({
            name: fileDto.name,
            owner: user._id,
            rootFolder,
        });
        if (foundFile) {
            throw new common_1.ConflictException('File with such a name already exists');
        }
        const fileId = new mongoose_1.Types.ObjectId();
        const uploadedFile = await this.fileService.uploadRemoteFile(fileId.toString(), file.buffer);
        const createdFile = await this.fileService.create({
            name: fileDto.name,
            owner: user._id,
            _id: fileId,
            isPrivate: fileDto.isPrivate,
            size: file.size,
            url: uploadedFile,
            rootFolder,
            ...(foundRootFolder && {
                permissions: foundRootFolder.permissions.filter((permit) => permit.email !== user.email),
            }),
        });
        if (createdFile) {
            await this.foldersService.findOneAndUpdate({
                _id: rootFolder,
                $or: [
                    { owner: user._id },
                    { 'permissions.email': user.email, 'permissions.role': 'editor' },
                ],
            }, {
                $push: { files: createdFile._id },
            });
        }
        return createdFile;
    }
};
exports.AddFileController = AddFileController;
__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiOperation)({ summary: 'Add a new file' }),
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiConsumes)('multipart/form-data'),
    (0, swagger_1.ApiBody)({
        schema: {
            type: 'object',
            properties: {
                name: {
                    description: 'File name',
                    type: 'string',
                    example: 'My file',
                },
                isPrivate: {
                    description: 'Indicates whether the file is private',
                    type: 'boolean',
                    example: true,
                },
                rootFolder: {
                    description: 'Root folder containing this file',
                    type: 'string',
                    example: '66471a9ce46805482dd323a4',
                },
                file: {
                    description: 'File',
                    type: 'string',
                    format: 'binary',
                },
            },
            required: ['name', 'rootFolder', 'file'],
        },
    }),
    (0, swagger_1.ApiCreatedResponse)({
        description: 'File successfully created',
        type: File_schema_1.File,
    }),
    (0, swagger_1.ApiBadRequestResponse)({
        description: 'Validation failed (expected size is less than 5000000)',
    }),
    (0, swagger_1.ApiNotFoundResponse)({
        description: 'Folder not found',
    }),
    (0, swagger_1.ApiForbiddenResponse)({
        description: 'Insufficient available memory to upload the file OR User does not have sufficient rights or permissions for this operation',
    }),
    (0, swagger_1.ApiConflictResponse)({
        description: 'File with such a name already exists',
    }),
    (0, swagger_1.ApiUnauthorizedResponse)({
        description: 'Invalid token or refreshToken or the user is not authenticated.',
    }),
    (0, common_1.UseGuards)(auth_guard_1.AuthGuard),
    (0, common_1.UseInterceptors)((0, platform_express_1.FileInterceptor)('file')),
    __param(0, (0, common_1.UploadedFile)(new common_1.ParseFilePipe({
        validators: [new common_1.MaxFileSizeValidator({ maxSize: 5000000 })],
    }))),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, currentUser_decorator_1.CurrentUser)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, File_dto_1.CreateFileDto,
        User_schema_1.User]),
    __metadata("design:returntype", Promise)
], AddFileController.prototype, "addFile", null);
exports.AddFileController = AddFileController = __decorate([
    (0, common_1.Controller)('files'),
    (0, swagger_1.ApiTags)('Files'),
    __metadata("design:paramtypes", [files_service_1.FilesService,
        folders_service_1.FoldersService])
], AddFileController);
//# sourceMappingURL=addFile.controller.js.map