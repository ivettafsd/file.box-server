/// <reference types="mongoose/types/aggregate" />
/// <reference types="mongoose/types/callback" />
/// <reference types="mongoose/types/collection" />
/// <reference types="mongoose/types/connection" />
/// <reference types="mongoose/types/cursor" />
/// <reference types="mongoose/types/document" />
/// <reference types="mongoose/types/error" />
/// <reference types="mongoose/types/expressions" />
/// <reference types="mongoose/types/helpers" />
/// <reference types="mongoose/types/middlewares" />
/// <reference types="mongoose/types/indexes" />
/// <reference types="mongoose/types/models" />
/// <reference types="mongoose/types/mongooseoptions" />
/// <reference types="mongoose/types/pipelinestage" />
/// <reference types="mongoose/types/populate" />
/// <reference types="mongoose/types/query" />
/// <reference types="mongoose/types/schemaoptions" />
/// <reference types="mongoose/types/schematypes" />
/// <reference types="mongoose/types/session" />
/// <reference types="mongoose/types/types" />
/// <reference types="mongoose/types/utility" />
/// <reference types="mongoose/types/validation" />
/// <reference types="mongoose/types/virtuals" />
/// <reference types="mongoose/types/inferschematype" />
import { Permission } from './../dto/Folder.dto';
import { FoldersService } from './../folders.service';
import { User } from 'src/schemas/User.schema';
import { Types } from 'mongoose';
import { FilesService } from 'src/files/files.service';
import { Folder } from 'src/schemas/Folder.schema';
export declare class ChangeFolderPermissionsController {
    private readonly foldersService;
    private readonly filesService;
    constructor(foldersService: FoldersService, filesService: FilesService);
    updateFolderPermissions({ id }: {
        id: any;
    }, folderDto: [Permission], user: User): Promise<import("mongoose").Document<unknown, {}, Folder> & Folder & Required<{
        _id: Types.ObjectId;
    }>>;
}
