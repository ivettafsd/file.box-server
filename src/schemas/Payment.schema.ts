import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document, Types } from 'mongoose';
import { Product } from './Product.schema';

@Schema({ versionKey: false, timestamps: true })
export class Payment extends Document {
  @ApiProperty({
    description: 'Payment ID',
    example: '66471a9ce46805482dd323a4',
  })
  @Prop({ required: true })
  _id: Types.ObjectId;

  @ApiProperty({
    description: 'Payer ID',
    example: '66471a9ce46805482dd323a4',
  })
  @Prop({ type: Types.ObjectId, ref: 'User', required: true })
  payer: Types.ObjectId;

  @ApiProperty({
    description: 'Payment amount',
    example: 1,
  })
  @Prop({ required: true, default: 1 })
  amount: number;

  @ApiProperty({
    description: 'Currency',
    example: 'PLN',
  })
  @Prop({ required: true, default: 'PLN' })
  currency: string;

  @ApiProperty({
    description: 'Product stripe ID',
    example: '66471a9ce46805482dd323a4',
  })
  @Prop({ type: Types.ObjectId, ref: 'Product', required: true })
  product: Types.ObjectId | Product;

  @ApiProperty({
    description: 'Status payment',
    example: 'success',
  })
  @Prop({
    required: true,
    default: 'pending',
    enum: ['pending', 'success', 'error'],
  })
  status: string;
}

export const PaymentSchema = SchemaFactory.createForClass(Payment);
