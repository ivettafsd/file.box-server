import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document, Types } from 'mongoose';

@Schema({ versionKey: false, timestamps: true })
export class Product extends Document {
  @ApiProperty({
    description: 'Product ID',
    example: '66471a9ce46805482dd323a4',
  })
  @Prop({ required: true })
  _id: Types.ObjectId;

  @ApiProperty({
    description: 'Product ID by Stripe',
    example: 'price_1PI4LZLEt9vDD8W3wH5iuQyO',
  })
  @Prop({ required: true, default: 'price_1PI4LZLEt9vDD8W3wH5iuQyO' })
  stripeID: string;

  @ApiProperty({
    description: 'Product name',
    example: '+10Mb',
  })
  @Prop({ required: true })
  name: string;

  @ApiProperty({
    description: 'Product price',
    example: 1,
  })
  @Prop({ required: true, default: 1 })
  price: number;

  @ApiProperty({
    description: 'Currency',
    example: 'PLN',
  })
  @Prop({ required: true, default: 'PLN' })
  currency: string;

  @ApiProperty({
    description: 'Product value in bytes',
    example: 10485760,
  })
  @Prop({ required: true, default: 10485760 })
  value: number;
}

export const ProductSchema = SchemaFactory.createForClass(Product);
