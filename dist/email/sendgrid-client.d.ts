import * as SendGrid from '@sendgrid/mail';
export declare class SendGridClient {
    private readonly logger;
    constructor();
    send(mail: SendGrid.MailDataRequired): Promise<void>;
}
