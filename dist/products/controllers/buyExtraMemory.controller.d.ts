import { User } from '../../schemas/User.schema';
import { UsersService } from '../../users/users.service';
import { ProductsService } from 'src/products/products.service';
import { CreateBuyMemoryDto } from '../../users/dto/User.dto';
export declare class BuyExtraMemoryController {
    private usersService;
    private productsService;
    constructor(usersService: UsersService, productsService: ProductsService);
    buyExtraMemory(createBuyMemoryDto: CreateBuyMemoryDto, user: User): Promise<{
        url: any;
    }>;
    paymentSuccess({ id }: {
        id: any;
    }, res: any): Promise<void>;
    paymentFailed({ id }: {
        id: any;
    }, res: any): Promise<void>;
}
