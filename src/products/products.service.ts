import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Product } from 'src/schemas/Product.schema';

@Injectable()
export class ProductsService {
  constructor(
    @InjectModel(Product.name) private productsModel: Model<Product>,
  ) {}
  async find(filters = {}) {
    return await this.productsModel.find(filters);
  }
  async findOne(filters = {}) {
    if (!filters) {
      return null;
    }
    return await this.productsModel.findOne(filters);
  }
}
