export declare class AuthUserDto {
    email: string;
    password: string;
    name?: string;
    avatar?: string;
}
export declare class ChangeUserPasswordDto {
    oldPassword: string;
    newPassword: string;
}
