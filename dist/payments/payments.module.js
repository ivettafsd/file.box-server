"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentsModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const Folder_schema_1 = require("../schemas/Folder.schema");
const User_schema_1 = require("../schemas/User.schema");
const jwt_1 = require("@nestjs/jwt");
const users_service_1 = require("../users/users.service");
const File_schema_1 = require("../schemas/File.schema");
const Product_schema_1 = require("../schemas/Product.schema");
const payments_service_1 = require("./payments.service");
const Payment_schema_1 = require("../schemas/Payment.schema");
let PaymentsModule = class PaymentsModule {
};
exports.PaymentsModule = PaymentsModule;
exports.PaymentsModule = PaymentsModule = __decorate([
    (0, common_1.Module)({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                { name: Payment_schema_1.Payment.name, schema: Payment_schema_1.PaymentSchema },
                {
                    name: Folder_schema_1.Folder.name,
                    schema: Folder_schema_1.FolderSchema,
                },
                {
                    name: User_schema_1.User.name,
                    schema: User_schema_1.UserSchema,
                },
                {
                    name: File_schema_1.File.name,
                    schema: File_schema_1.FileSchema,
                },
                { name: Product_schema_1.Product.name, schema: Product_schema_1.ProductSchema },
            ]),
        ],
        controllers: [],
        providers: [jwt_1.JwtService, users_service_1.UsersService, payments_service_1.PaymentsService],
    })
], PaymentsModule);
//# sourceMappingURL=payments.module.js.map