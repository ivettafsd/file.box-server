import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Folder, FolderSchema } from '../schemas/Folder.schema';
import { User, UserSchema } from '../schemas/User.schema';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { File, FileSchema } from 'src/schemas/File.schema';
import { Product, ProductSchema } from 'src/schemas/Product.schema';
import { GetProductsController } from './controllers/getProducts.controller';
import { ProductsService } from './products.service';
import { BuyProductController } from './controllers/buyProduct.controller';
import { PaymentsService } from 'src/payments/payments.service';
import { Payment, PaymentSchema } from 'src/schemas/Payment.schema';
import { BuyProductSuccessController } from './controllers/buyProductSuccess.controller';
import { BuyProductFailedController } from './controllers/buyProductFailed.controller';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: Folder.name,
        schema: FolderSchema,
      },
      {
        name: User.name,
        schema: UserSchema,
      },
      {
        name: File.name,
        schema: FileSchema,
      },
      { name: Product.name, schema: ProductSchema },
      { name: Payment.name, schema: PaymentSchema },
    ]),
  ],
  controllers: [
    GetProductsController,
    BuyProductController,
    BuyProductSuccessController,
    BuyProductFailedController,
  ],
  providers: [JwtService, UsersService, ProductsService, PaymentsService],
})
export class ProductsModule {}
