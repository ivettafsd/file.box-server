import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Types } from 'mongoose';

@Schema({ versionKey: false, timestamps: true })
export class User {
  @ApiProperty({ description: 'User ID', example: '66471a9ce46805482dd323a4' })
  @Prop({ required: true })
  _id: Types.ObjectId;

  @ApiProperty({
    description: 'User email',
    example: 'test@gmail.com',
  })
  @Prop({ unique: true, required: true })
  email: string;

  @Prop({ required: true })
  password?: string;

  @ApiProperty({ description: 'User name', example: 'Ivetta' })
  @Prop({ required: true })
  name: string;

  @ApiProperty({
    description: 'User avatar',
    example:
      'https://ivetta-dashkova-file-box.s3.eu-north-1.amazonaws.com/unknown.png',
  })
  @Prop({
    required: true,
    default:
      'https://ivetta-dashkova-file-box.s3.eu-north-1.amazonaws.com/unknown.png',
  })
  avatar: string;

  @ApiProperty({
    description: 'User access token',
    example:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY2NDcxYTljZTQ2ODA1NDgyZGQzMjNhNCIsImlhdCI6MTcxNTkzNTkwMSwiZXhwIjoxNzE1OTM2MjAxfQ.LKYRlc6MYzauS2vpZt31wfFRGGmu2VqZVJEi-L4fqE4',
  })
  @Prop({ required: false })
  token: string;

  @ApiProperty({
    description: 'User refresh token',
    example:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY2NDcxYTljZTQ2ODA1NDgyZGQzMjNhNCIsImlhdCI6MTcxNTkzNTkwMSwiZXhwIjoxNzE2NTQwNzAxfQ.AjlkO1MRuYNINnk9B3P_flY7RFyrzoURFKMxn8nDEGI',
  })
  @Prop({ required: false })
  refreshToken: string;

  @ApiProperty({
    description: 'User main folder ID',
    example: '66471a9ce46805482dd323a4',
  })
  @Prop({ required: false })
  mainFolder: Types.ObjectId;

  @ApiProperty({
    description: 'Available memory for the user to save files',
    example: 10485760,
  })
  @Prop({ required: true, default: 10485760 })
  availableMemory: number;

  @ApiProperty({
    description: 'User creation timestamp',
    example: '2024-01-01T00:00:00.000Z',
  })
  createdAt: Date;

  @ApiProperty({
    description: 'User update timestamp',
    example: '2024-01-01T00:00:00.000Z',
  })
  updatedAt: Date;
}

export const UserSchema = SchemaFactory.createForClass(User);
