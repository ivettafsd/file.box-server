import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Types, Document } from 'mongoose';

class Permission {
  @Prop({ required: true })
  role: string;

  @Prop({ required: true })
  email: string;
}

@Schema({ versionKey: false, timestamps: true })
export class File extends Document {
  @ApiProperty({
    description: 'File ID',
    example: '66471a9ce46805482dd323a4',
  })
  @Prop({ required: true })
  _id: Types.ObjectId;

  @ApiProperty({
    description: 'URL of the file',
    example: 'https://example.com/file.pdf',
  })
  @Prop({ unique: true, required: true })
  url: string;

  @ApiProperty({
    description: 'Root folder containing this folder',
    example: '66471a9ce46805482dd323a4',
  })
  @Prop({ required: false })
  rootFolder: string;

  @ApiProperty({
    description: 'Size of the file in bytes',
    example: 1024,
  })
  @Prop({ required: true })
  size: number;

  @ApiProperty({
    description: 'File name',
    example: 'My file',
  })
  @Prop({ required: true })
  name: string;

  @ApiProperty({
    description: 'Indicates whether the file is private',
    example: false,
  })
  @Prop({ required: true, default: true })
  isPrivate: boolean;

  @ApiProperty({
    description: 'Owner of the file (User ID)',
    example: '66471a9ce46805482dd323a4',
  })
  @Prop({ type: Types.ObjectId, ref: 'User', required: true })
  owner: Types.ObjectId;

  @ApiProperty({
    description: 'Permissions granted to users for accessing the file',
    example: [{ role: 'viewer', email: 'user@example.com' }],
  })
  @Prop([Permission])
  permissions: Permission[] = [];
}

export const FileSchema = SchemaFactory.createForClass(File);
