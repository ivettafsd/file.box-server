export declare class CreatePaymentDto {
    payer: string;
    password: string;
    name?: string;
    avatar?: string;
}
