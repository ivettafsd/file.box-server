import { PaymentsService } from 'src/payments/payments.service';
export declare class BuyProductFailedController {
    private paymentsService;
    constructor(paymentsService: PaymentsService);
    paymentFailed({ id }: {
        id: any;
    }): Promise<{
        url: string;
    }>;
}
