"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const common_1 = require("@nestjs/common");
const jwt_1 = require("@nestjs/jwt");
const email_service_1 = require("../email/email.service");
const bcrypt = require("bcrypt");
const mongoose_1 = require("mongoose");
const users_service_1 = require("./users.service");
const folders_service_1 = require("../folders/folders.service");
const { SECRET_KEY, REFRESH_SECRET_KEY, PROJECT_EMAIL, PROJECT } = process.env;
let AuthService = class AuthService {
    constructor(usersService, foldersService, jwtService, emailService) {
        this.usersService = usersService;
        this.foldersService = foldersService;
        this.jwtService = jwtService;
        this.emailService = emailService;
    }
    async signUp(createDto) {
        const user = await this.usersService.findOne({
            email: createDto.email,
        });
        if (user) {
            throw new common_1.ConflictException('Email is already in use');
        }
        const hashPassword = await bcrypt.hash(createDto.password, 10);
        const userId = new mongoose_1.Types.ObjectId();
        const createdUser = await this.usersService.create({
            email: createDto.email,
            password: hashPassword,
            name: createDto.name || createDto.email.split('@')[0],
            ...(createDto.avatar && { avatar: createDto.avatar }),
            _id: userId,
        });
        const mainFolder = await this.foldersService.create({
            title: 'Main folder',
            owner: createdUser._id,
            _id: createdUser._id,
        });
        const updatedUser = await this.usersService.update(createdUser._id, {
            mainFolder: mainFolder._id,
        });
        return updatedUser;
    }
    async signIn(createDto) {
        const user = await this.usersService.findOne({
            email: createDto.email,
        });
        if (!user) {
            throw new common_1.NotFoundException('User with such an email not found');
        }
        const isCorrectPassword = await bcrypt.compare(createDto.password, user.password);
        if (!isCorrectPassword) {
            throw new common_1.BadRequestException('Email or password invalid');
        }
        const refreshedUser = await this.refreshTokens(user._id);
        return refreshedUser;
    }
    async changeUserPassword(changePasswordDto, user) {
        const foundUser = await this.usersService.findOne({
            _id: user._id,
        });
        const isCorrectPassword = await bcrypt.compare(changePasswordDto.oldPassword, foundUser.password);
        if (!isCorrectPassword) {
            throw new common_1.BadRequestException('Email or password invalid');
        }
        const hashPassword = await bcrypt.hash(changePasswordDto.newPassword, 10);
        const updatedUser = await this.usersService.update(user._id, {
            password: hashPassword,
        });
        return updatedUser;
    }
    async refreshTokens(userId) {
        const payload = { id: userId };
        const token = await this.jwtService.signAsync(payload, {
            secret: SECRET_KEY,
            expiresIn: '1h',
        });
        const refreshToken = await this.jwtService.signAsync(payload, {
            secret: REFRESH_SECRET_KEY,
            expiresIn: '7d',
        });
        const updatedUser = await this.usersService.update(userId, {
            token,
            refreshToken,
        });
        return updatedUser;
    }
    async signOut(userId) {
        const updatedUser = await this.usersService.update(userId, {
            token: null,
            refreshToken: null,
        });
        return updatedUser;
    }
    async googleLogin(req) {
        if (!req.user) {
            return null;
        }
        const foundUser = await this.usersService.findOne({
            email: req.user.email,
        });
        let userId;
        if (!foundUser) {
            const newPassword = new mongoose_1.Types.ObjectId().toString();
            const registeredUser = await this.signUp({
                email: req.user.email,
                name: req.user.name,
                avatar: req.user.avatar,
                password: newPassword,
            });
            await this.emailService.sendEmailWithTemplate(req.user.email, {
                email: req.user.email,
                password: newPassword,
                name: req.user.name,
                project: PROJECT,
                support: PROJECT_EMAIL,
            }, 'You are signed in to File.box with Google', 'google');
            userId = registeredUser._id;
        }
        else {
            userId = foundUser._id;
        }
        const authorizedUser = await this.refreshTokens(userId);
        return authorizedUser;
    }
};
exports.AuthService = AuthService;
exports.AuthService = AuthService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [users_service_1.UsersService,
        folders_service_1.FoldersService,
        jwt_1.JwtService,
        email_service_1.EmailService])
], AuthService);
//# sourceMappingURL=auth.service.js.map