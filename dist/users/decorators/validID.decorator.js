"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ValidId = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
exports.ValidId = (0, common_1.createParamDecorator)((data, ctx) => {
    const request = ctx.switchToHttp().getRequest();
    const id = request.params.id;
    if (!(0, mongoose_1.isValidObjectId)(id)) {
        throw new common_1.BadRequestException('Invalid ID');
    }
    return id;
});
//# sourceMappingURL=validID.decorator.js.map