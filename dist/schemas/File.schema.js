"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileSchema = exports.File = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const swagger_1 = require("@nestjs/swagger");
const mongoose_2 = require("mongoose");
class Permission {
}
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], Permission.prototype, "role", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], Permission.prototype, "email", void 0);
let File = class File extends mongoose_2.Document {
    constructor() {
        super(...arguments);
        this.permissions = [];
    }
};
exports.File = File;
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'File ID',
        example: '66471a9ce46805482dd323a4',
    }),
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", mongoose_2.Types.ObjectId)
], File.prototype, "_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'URL of the file',
        example: 'https://example.com/file.pdf',
    }),
    (0, mongoose_1.Prop)({ unique: true, required: true }),
    __metadata("design:type", String)
], File.prototype, "url", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'Root folder containing this folder',
        example: '66471a9ce46805482dd323a4',
    }),
    (0, mongoose_1.Prop)({ required: false }),
    __metadata("design:type", String)
], File.prototype, "rootFolder", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'Size of the file in bytes',
        example: 1024,
    }),
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", Number)
], File.prototype, "size", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'File name',
        example: 'My file',
    }),
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], File.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'Indicates whether the file is private',
        example: false,
    }),
    (0, mongoose_1.Prop)({ required: true, default: true }),
    __metadata("design:type", Boolean)
], File.prototype, "isPrivate", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'Owner of the file (User ID)',
        example: '66471a9ce46805482dd323a4',
    }),
    (0, mongoose_1.Prop)({ type: mongoose_2.Types.ObjectId, ref: 'User', required: true }),
    __metadata("design:type", mongoose_2.Types.ObjectId)
], File.prototype, "owner", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'Permissions granted to users for accessing the file',
        example: [{ role: 'viewer', email: 'user@example.com' }],
    }),
    (0, mongoose_1.Prop)([Permission]),
    __metadata("design:type", Array)
], File.prototype, "permissions", void 0);
exports.File = File = __decorate([
    (0, mongoose_1.Schema)({ versionKey: false, timestamps: true })
], File);
exports.FileSchema = mongoose_1.SchemaFactory.createForClass(File);
//# sourceMappingURL=File.schema.js.map