import { User } from '../../schemas/User.schema';
import { ProductsService } from 'src/products/products.service';
import { PaymentsService } from 'src/payments/payments.service';
export declare class BuyProductController {
    private productsService;
    private paymentsService;
    constructor(productsService: ProductsService, paymentsService: PaymentsService);
    buyExtraMemory({ id }: {
        id: any;
    }, user: User): Promise<{
        url: string;
    }>;
}
